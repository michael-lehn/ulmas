#include <stdio.h>

#include "memregion.h"
#include "symtab.h"

struct Entry
{
    struct Entry *next;
    const char *sym;
    struct Expr *expr;
    size_t useCount;
    bool global;
};

static struct Entry *symTab;
static size_t countEntriesWithExpr;

static struct Entry *
findEntry(const char *sym)
{
    for (struct Entry *l = symTab; l; l = l->next) {
	if (l->sym == sym) {
	    return l;
	}
    }
    return 0;
}

struct Entry *
addSym(const char *sym, struct Expr *expr)
{
    struct Entry *found = findEntry(sym);

    if (found) {
	if (!found->expr && expr) {
	    ++countEntriesWithExpr;
	}
	if (found->useCount == 0 && found->expr) {
	    *found->expr = *expr;
	} else {
	    found->expr = expr;
	}
	return found;
    }

    struct Entry *newEntry = alloc(SYMTAB, sizeof(*newEntry));
    newEntry->next = symTab;
    newEntry->sym = sym;
    newEntry->expr = expr;
    newEntry->useCount = 0;
    newEntry->global = false;
    symTab = newEntry;
    if (expr) {
	++countEntriesWithExpr;
    }
    return symTab;
}

void
globalSym(const char *sym)
{
    struct Entry *found = findEntry(sym);

    if (! found) {
	found = addSym(sym, 0);
    }
    found->global = true;
}

void
resolveSym(const char *sym, struct Expr *expr)
{
    struct Entry *found = findEntry(sym);

    if (found && found->expr) {
	*found->expr = *expr;
	return;
    }
    addSym(sym, expr);
}

struct Expr *
getSym(const char *sym)
{
    struct Entry *found = findEntry(sym);
    if (found) {
	return found->expr;
    }
    return 0;
}

enum ExprType
getSymType(const char *sym)
{
    struct Entry *found = findEntry(sym);
    if (found && found->expr) {
	return found->expr->type;
    }
    return UNKNOWN;
}

void
fixSymtab()
{
    size_t fixed;
    do {
	fixed = 0;
	for (struct Entry *l = symTab; l; l = l->next) {
	    fixed += fixExpressionType(l->expr);
	}
    } while (fixed);
}

void
fprintSymTab(FILE *out)
{
    if (! countEntriesWithExpr) {
	return;
    }

    fprintf(out, "#SYMTAB\n");
    for (struct Entry *l = symTab; l; l = l->next) {
	if (! l->expr) {
	    printf("waring: symtab entry without expression\n");
	    continue;
	}

	switch (l->expr->type) {
	    case TEXT:
		l->global ? fprintf(out, "T") : fprintf(out, "t");
		break;
	    case DATA:
		l->global ? fprintf(out, "D") : fprintf(out, "d");
		break;
	    case BSS:
		l->global ? fprintf(out, "B") : fprintf(out, "b");
		break;
	    case ABSOLUTE:
		l->global ? fprintf(out, "A") : fprintf(out, "a");
		break;
	    case UNKNOWN:
		fprintf(out, "U");
		break;
	    default:
		fprintf(stderr, "internal error in symtab.\n");
	}

	uint64_t val = evalExpression(l->expr);
	
	fprintf(out, " %-27s 0x%016llx", l->sym, val);
	fprintf(out, "\n");
    }
}
