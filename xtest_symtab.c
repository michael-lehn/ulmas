#include "string.h"
#include "symtab.h"

int
main()
{
    const char *sym1 = addString("a");
    struct Expr *expr1 = makeValExpr(0, ABSOLUTE, 42, "42");

    const char *sym2 = addString("b");
    struct Expr *expr2 = makeValExpr(0, ABSOLUTE, 2, "2");

    const char *sym3 = addString("c");
    struct Expr *expr3 = makeValExpr(0, ABSOLUTE, 4, "4");

    const char *sym4 = addString("a");
    struct Expr *expr4 = makeValExpr(0, ABSOLUTE, 21, "21");
    expr4 = makeBinaryExpr(MUL, expr4, expr1);


    addSym(sym1, expr1);
    addSym(sym2, expr2);
    addSym(sym3, expr3);
    addSym(sym4, expr4);

    fprintSymTab(stdout);
}
