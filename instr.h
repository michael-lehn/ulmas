#ifndef ULMAS_INSTR_H
#define ULMAS_INSTR_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "lexer.h"

enum InstrFmt {
    FMT_,
    FMT_I,
    FMT_R,
    FMT_IR,
    FMT_RR,
    FMT_IRR,
    FMT_RRR,
    FMT_IMR_R,
    FMT_MRR1_R,
    FMT_MRR2_R,
    FMT_MRR4_R,
    FMT_MRR8_R,
    FMT_R_IMR,
    FMT_R_MRR1,
    FMT_R_MRR2,
    FMT_R_MRR4,
    FMT_R_MRR8,
    FMT_STRING,
};

struct InstrOperand
{
    enum InstrFmt fmt;

    union {
	struct Expr *expr[5];
	const char *str[2];
    } u;
};

struct InstrOperand *make1AddrOperand(enum InstrFmt fmt,
				      struct Expr *expr0);

struct InstrOperand *make2AddrOperand(enum InstrFmt fmt,
				      struct Expr *expr0,
				      struct Expr *expr1);

struct InstrOperand *make3AddrOperand(enum InstrFmt fmt,
				      struct Expr *expr0,
				      struct Expr *expr1,
				      struct Expr *expr2);

struct InstrOperand *make5AddrOperand(enum InstrFmt fmt,
	 			      struct Expr *expr0,
				      struct Expr *expr1,
				      struct Expr *expr2,
				      struct Expr *expr3,
				      struct Expr *expr4);

struct InstrOperand *fixFetchOperand(struct InstrOperand *operand,
				     struct Expr *dest);

struct InstrOperand *fixStoreOperand(struct InstrOperand *operand,
				     struct Expr *dest);

// TODO: keep the location of the instruction for better error messages during
// code generation.
struct Instr
{
    enum TokenKind mnemonic;
    struct InstrOperand *operand;
    uint64_t addr;
    size_t size;

    struct Instr *next;
};

extern struct Instr *segment[3];

void assembleText();
void assembleData();
void assembleBss();
void setAlignment(size_t align);
size_t getSegmentAlignment(size_t seg);
size_t getSegmentSize(size_t seg);
size_t getSegmentOffset(size_t seg);
bool setLabel(const struct TokenLoc *loc, const char *ident);

struct Instr *makeInstr(enum TokenKind mnemonic,
			struct InstrOperand *operand);

struct Instr *makeStringPseudoOp(const char *processedStr, const char *str);

void fprintInstr(FILE *out, const struct Instr *instr);

#endif // ULMAS_NODES_H
