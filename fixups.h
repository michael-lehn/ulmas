#ifndef ULMAS_FIXUPS_H
#define ULMAS_FIXUPS_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include "expr.h"

enum FixupType
{
    FIXUP_ABSOLUTE,
    FIXUP_RELATIVE,
    FIXUP_W0_,		// for internal use only
    FIXUP_W1_,
    FIXUP_W2_,
    FIXUP_W3_,
};

uint64_t extractValue(size_t seg, const struct Expr *expr, uint64_t addr,
		      size_t offset, size_t numBytes, enum FixupType fixupType);

void fprintFixupTab(FILE *out);

#endif // ULMAS_FIXUPS_H
