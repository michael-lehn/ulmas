#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"
#include "msg.h"
#include "stop.h"

#ifndef LEXER_TOKEN_BUFFER_SIZE
#define LEXER_TOKEN_BUFFER_SIZE 256
#endif

#ifndef LEXER_LINE_BUFFER_SIZE
#define LEXER_LINE_BUFFER_SIZE 256
#endif

enum ErrorCode {
    OP_NOT_IMPLEMENTED,
    SINGLE_QUOTE_AS_CHARACTER_LITERAL,
    NEWLINE_AS_CHARACTER_LITERAL,
    EXPECTED_HEXDIGIT,
    INVALID_CHARACTER_LITERAL,
    EOF_IN_CHARACTER_LITERAL,
    NEWLINE_IN_STRING_LITERAL,
    EOF_IN_STRING_LITERAL,
    INVALID_HEXADECIMAL_LITERAL,
    EOF_IN_DELIMITED_COMMENT,
    BAD_TOKEN,
    EXPECTED_MNEMONIC,
    EXPECTED_LABEL,
};

static const char *errorStr[] = {
    // OP_NOT_IMPLEMENTED
    "not your fault. Operation '%s' is currently not implemented",

    // SINGLE_QUOTE_AS_CHARACTER_LITERAL
    "single quote charater is not permitted as character constant",

    // NEWLINE_AS_CHARACTER_LITERAL,
    "newline not permitted in character constant",

    // EXPECTED_HEXDIGIT
    "hexadecimal digit expected",

    // INVALID_CHARACTER_LITERAL
    "invalid character constant",

    // EOF_IN_CHARACTER_LITERAL
    "end of file in character constant",

    // NEWLINE_IN_STRING_LITERAL
    "newline not permitted in string constant",

    // EOF_IN_STRING_LITERAL
    "end of file in string constant",

    // INVALID_HEXADECIMAL_LITERAL
    "invalid hexadecimal constant",

    // EOF_IN_DELIMITED_COMMENT
    "unexpected eof in delimited comment",

    // BAD_TOKEN
    "bad token",

    // EXPECTED_MNEMONIC
    "mnemonic expected",

    // EXPECTED_LABEL
    "label expected",
};


enum TokenKind tokenKind = EOL;
struct TokenLoc tokenLoc;

static struct TokenPos pos = {1, 1};
static int ch;
static FILE *in_;

struct TokenValue
{
    char buf[LEXER_TOKEN_BUFFER_SIZE];
    char *cp;
};

static struct TokenValue tokenValue_, processedTokenValue_;

const char *tokenValue = tokenValue_.buf;
const char *processedTokenValue = processedTokenValue_.buf;

static void
tokenValue_disable(struct TokenValue *tokenValue)
{
    tokenValue->cp = 0;
}

static void
tokenValue_clear(struct TokenValue *tokenValue)
{
    tokenValue->cp = tokenValue->buf;
    *(tokenValue->cp) = 0;
}

static void
tokenValue_append(struct TokenValue *tokenValue, int ch)
{
    if (! tokenValue->cp) {
	return;
    }
    *(tokenValue->cp++) = ch;
    if (tokenValue->cp - tokenValue->buf >= LEXER_TOKEN_BUFFER_SIZE) {
	fprintf(stderr, "limit of %d bytes for tokenValue exeeded.\n",
		LEXER_TOKEN_BUFFER_SIZE);
	fprintf(stderr, "last character appended was '%c'\n", ch);
	stop(2);
    }
    *(tokenValue->cp) = 0;
}

struct InputLine {
    char buffer[LEXER_LINE_BUFFER_SIZE];
    char *cp, *end;
    size_t lineNumber;
    bool eof;
};

static struct InputLine inputLine_[2], *inputLine = &inputLine_[0];

static void
fillLineBuffer()
{
    size_t nextLineNumber = inputLine->lineNumber + 1;
    ptrdiff_t i = inputLine - inputLine_;

    inputLine = &inputLine_[1 - i];

    inputLine->cp = inputLine->end = inputLine->buffer;
    inputLine->lineNumber = nextLineNumber;

    int ch;

    do {
	if (inputLine->end - inputLine->buffer >= LEXER_LINE_BUFFER_SIZE) {
	    fprintf(stderr, "limit of %d bytes for line buffer exeeded.\n",
		    LEXER_LINE_BUFFER_SIZE);
	    stop(2);
	}
	ch = fgetc(in_);
	if (ch == EOF) {
	    inputLine->eof = true;
	    break;
	}
	*inputLine->end++ = ch;
    } while (ch != '\n');

    *inputLine->end = 0;
}

const char *
currentInputLine()
{
    return inputLine->buffer;
}

size_t
currentInputLineNumber()
{
    return inputLine->lineNumber;
}

const char *
previousInputLine()
{
    ptrdiff_t i = inputLine - inputLine_;
    const struct InputLine *prev = &inputLine_[1 - i];
    return prev->lineNumber ? prev->buffer : 0;
}

size_t
previousInputLineNumber()
{
    ptrdiff_t i = inputLine - inputLine_;
    const struct InputLine *prev = &inputLine_[1 - i];
    return prev->lineNumber;
}

static const char *tokenKindStr_[] = {
    "EOI",
    "EMPTYLABEL",
    "UNKNOWN_TOKEN",
    "DECIMAL_LITERAL",
    "OCTAL_LITERAL",
    "HEXADECIAML_LITERAL",
    "CHARACTER_LITERAL",
    "STRING_LITERAL",
    "IDENT",
    "EOL",
    "PLUS",
    "MINUS",
    "ASTERISK",
    "SLASH",
    "PERCENT",
    "COLON",
    "COMMA",
    "DOLLAR",
    "LPAREN",
    "LPAREN_MEM",
    "RPAREN",
    "W0",
    "W1",
    "W2",
    "W3",

    "ADDQ",
    "ANDQ",
    "DIVQ",
    "GETC",
    "HALT",
    "IDIVQ",
    "IMULQ",
    "JA",
    "JAE",
    "JB",
    "JBE",
    "JE",
    "JG",
    "JGE",
    "JL",
    "JLE",
    "JMP",
    "JNA",
    "JNAE",
    "JNB",
    "JNBE",
    "JNE",
    "JNG",
    "JNGE",
    "JNL",
    "JNLE",
    "JNZ",
    "JZ",
    "LDSWQ",
    "LDZWQ",
    "MOVB",
    "MOVL",
    "MOVQ",
    "MOVSBQ",
    "MOVSLQ",
    "MOVSWQ",
    "MOVW",
    "MOVZBQ",
    "MOVZLQ",
    "MOVZWQ",
    "MULQ",
    "NOP",
    "NOTQ",
    "ORQ",
    "PUTC",
    "SALQ",
    "SARQ",
    "SHLDWQ",
    "SHLQ",
    "SHRQ",
    "SUBQ",
    "TRAP",

    "DOT_ALIGN",
    "DOT_BSS",
    "DOT_BYTE",
    "DOT_DATA",
    "DOT_EQU",
    "DOT_EQUIV",
    "DOT_GLOBL",
    "DOT_GLOBAL",
    "DOT_LONG",
    "DOT_QUAD",
    "DOT_SET",
    "DOT_SPACE",
    "DOT_STRING",
    "DOT_TEXT",
    "DOT_WORD",
};

static const char *mnemonicStr_[] = {
    "<EOI>",
    "<EMPTY_LABEL>",
    "<UNKNOWN_TOKEN>",
    "<DECIMAL_LITERAL>",
    "<OCTAL_LITERAL>",
    "<HEXADECIMAL_LITERAL>",
    "<CHARACTER_LITERAL>",
    "<STRING_LITERAL>",
    "<IDENT>",
    "\\n",
    "+",
    "-",
    "*",
    "/",
    "%",
    ":",
    ",",
    "$",
    "(",
    "(%",
    ")",
    "@0",
    "@1",
    "@2",
    "@3",

    "addq",
    "andq",
    "divq",
    "getc",
    "halt",
    "idivq",
    "imulq",
    "ja",
    "jae",
    "jb",
    "jbe",
    "je",
    "jg",
    "jge",
    "jl",
    "jle",
    "jmp",
    "jna",
    "jnae",
    "jnb",
    "jnbe",
    "jne",
    "jng",
    "jnge",
    "jnl",
    "jnle",
    "jnz",
    "jz",
    "ldswq",
    "ldzwq",
    "movb",
    "movl",
    "movq",
    "movsbq",
    "movslq",
    "movswq",
    "movw",
    "movzbq",
    "movzlq",
    "movzwq",
    "mulq",
    "nop",
    "notq",
    "orq",
    "putc",
    "salq",
    "sarq",
    "shldwq",
    "shlq",
    "shrq",
    "subq",
    "trap",

    ".align",
    ".bss",
    ".byte",
    ".data",
    ".equ",
    ".equiv",
    ".globl",
    ".global",
    ".long",
    ".quad",
    ".set",
    ".space",
    ".string",
    ".text",
    ".word",
};

const char *
tokenKindStr(enum TokenKind tokenKind)
{
    return tokenKindStr_[tokenKind];
}

const char *
mnemonicStr(enum TokenKind tokenKind)
{
    return mnemonicStr_[tokenKind];
}

static bool
tokenFixIdentToMnemomic()
{
    if (tokenKind != IDENT) {
	return false;
    }

    size_t numKeywords =  sizeof(mnemonicStr_) / sizeof(mnemonicStr_[0]);
    for (size_t i = 0; i < numKeywords; ++i) {
	if (! strcmp(tokenValue, mnemonicStr_[i])) {
	    tokenKind = i;
	    return true;
	}
    }
    return false;
}

enum FieldPos
{
    EolField = 0,
    LabelField,
    MnemonicField,
    OpField,
};

static enum FieldPos fieldPos;

static void
lexError(enum ErrorCode errorCode)
{
    errorMsg(&tokenLoc, errorStr[errorCode]);
    stop(1);
}

bool
is_space(char ch)
{
    return ch == ' ' || ch == '\r' || ch == '\f' || ch == '\v' || ch == '\t';
}

bool
is_letter(char ch)
{
    return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || ch == '.'
	|| ch == '_';
}

bool
is_digit(char ch)
{
    return ch >= '0' && ch <= '9';
}

bool
is_octaldigit(char ch)
{
    return ch >= '0' && ch <= '7';
}

bool
is_hexdigit(char ch)
{
    return (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f')
	|| (ch >= 'A' && ch <= 'F');
}

unsigned
hex_to_val(char ch)
{
    if (ch >= '0' && ch <= '9') {
	return ch - '0';
    } else if (ch >= 'a' && ch <= 'f') {
	return 10 + ch - 'a';
    } else {
	return 10 + ch - 'A';
    }
}

static void
nextch()
{
    tokenLoc.end = pos;
    tokenValue_append(&tokenValue_, ch);

    if (! inputLine->cp || inputLine->cp == inputLine->end) {
	fillLineBuffer();
    }

    if (inputLine->cp == inputLine->end) {
	if (inputLine->eof) {
	    ch = EOF;
	    return;
	}
    }

    ch = *inputLine->cp++;

    if (ch == '\n') {
	pos.col = 1;
	++pos.line;
    } else if (ch == '\t') {
	pos.col += 8 - pos.col % 8;
    } else {
	++pos.col;
    }
}

void
parseCharacterLiteral()
{
    tokenValue_clear(&processedTokenValue_);
    // ch == '\''
    nextch();
    if (ch == '\'') {
	lexError(SINGLE_QUOTE_AS_CHARACTER_LITERAL);
    }
    do {
	if (ch == '\n') {
	    lexError(NEWLINE_AS_CHARACTER_LITERAL);
	} else if (ch == '\\') {
	    nextch();
	    if (is_octaldigit(ch)) {
		unsigned octalval = ch - '0';
		nextch();
		if (is_octaldigit(ch)) {
		    octalval = octalval * 8 + ch - '0';
		    nextch();
		}
		if (is_octaldigit(ch)) {
		    octalval = octalval * 8 + ch - '0';
		    nextch();
		}
		tokenValue_append(&processedTokenValue_, octalval);
	    } else {
		switch (ch) {
		    /* simple-escape-sequence */
		    case '\'':
			tokenValue_append(&processedTokenValue_, '\'');
			nextch(); break;
		    case '"':
			tokenValue_append(&processedTokenValue_, '\"');
			nextch(); break;
		    case '?':
			tokenValue_append(&processedTokenValue_, '\?');
			nextch(); break;
		    case '\\':
			tokenValue_append(&processedTokenValue_, '\\');
			nextch(); break;
		    case 'a':
			tokenValue_append(&processedTokenValue_, '\a');
			nextch(); break;
		    case 'b':
			tokenValue_append(&processedTokenValue_, '\b');
			nextch(); break;
		    case 'f':
			tokenValue_append(&processedTokenValue_, '\f');
			nextch(); break;
		    case 'n':
			tokenValue_append(&processedTokenValue_, '\n');
			nextch(); break;
		    case 'r':
			tokenValue_append(&processedTokenValue_, '\r');
			nextch(); break;
		    case 't':
			tokenValue_append(&processedTokenValue_, '\t');
			nextch(); break;
		    case 'v':
			tokenValue_append(&processedTokenValue_, '\v');
			nextch(); break;
		    case 'x':
			{
			    nextch();
			    if (!is_hexdigit(ch)) {
				lexError(EXPECTED_HEXDIGIT);
			    }
			    unsigned hexval = hex_to_val(ch);
			    nextch();
			    while (is_hexdigit(ch)) {
				hexval = hexval * 16 + hex_to_val(ch);
				nextch();
			    }
			    tokenValue_append(&processedTokenValue_, hexval);
			    break;
			}
		    default:
			lexError(INVALID_CHARACTER_LITERAL);
		}
	    }
	} else if (ch == EOF) {
	    lexError(EOF_IN_CHARACTER_LITERAL);
	} else {
	    tokenValue_append(&processedTokenValue_, ch);
	    nextch();
	}
    } while (ch != '\'');
    nextch();
}

void
parseStringLiteral()
{
    tokenValue_clear(&processedTokenValue_);
    // ch == '"'
    nextch();
    while (ch != '"') {
	if (ch == '\n') {
	    lexError(NEWLINE_IN_STRING_LITERAL);
	} else if (ch == '\\') {
	    nextch();
	    if (is_octaldigit(ch)) {
		unsigned octalval = ch - '0';
		nextch();
		if (is_octaldigit(ch)) {
		    octalval = octalval * 8 + ch - '0';
		    nextch();
		}
		if (is_octaldigit(ch)) {
		    octalval = octalval * 8 + ch - '0';
		    nextch();
		}
		ch = octalval;
		tokenValue_append(&processedTokenValue_, ch);
	    } else {
		switch (ch) {
		    /* simple-escape-sequence */
		    case '\'':
			tokenValue_append(&processedTokenValue_, '\'');
			nextch(); break;
		    case '"':
			tokenValue_append(&processedTokenValue_, '\"');
			nextch(); break;
		    case '?':
			tokenValue_append(&processedTokenValue_, '\?');
			nextch(); break;
		    case '\\':
			tokenValue_append(&processedTokenValue_, '\\');
			nextch(); break;
		    case 'a':
			tokenValue_append(&processedTokenValue_, '\a');
			nextch(); break;
		    case 'b':
			tokenValue_append(&processedTokenValue_, '\b');
			nextch(); break;
		    case 'f':
			tokenValue_append(&processedTokenValue_, '\f');
			nextch(); break;
		    case 'n':
			tokenValue_append(&processedTokenValue_, '\n');
			nextch(); break;
		    case 'r':
			tokenValue_append(&processedTokenValue_, '\r');
			nextch(); break;
		    case 't':
			tokenValue_append(&processedTokenValue_, '\t');
			nextch(); break;
		    case 'v':
			tokenValue_append(&processedTokenValue_, '\v');
			nextch(); break;
		    case 'x':
			{
			    nextch();
			    if (!is_hexdigit(ch)) {
				lexError(EXPECTED_HEXDIGIT);
			    }
			    unsigned hexval = hex_to_val(ch);
			    nextch();
			    while (is_hexdigit(ch)) {
				hexval = hexval * 16 + hex_to_val(ch);
				nextch();
			    }
			    tokenValue_append(&processedTokenValue_, hexval);
			    break;
			}
		    default:
			lexError(INVALID_CHARACTER_LITERAL);
		}
	    }
	} else if (ch == EOF) {
	    lexError(EOF_IN_STRING_LITERAL);
	} else {
	    tokenValue_append(&processedTokenValue_, ch);
	    nextch();
	}
   }
   nextch();
}

enum TokenKind
getToken_()
{
    if (! ch) {
	nextch();
	tokenLoc.begin = tokenLoc.end;
    }

    if (fieldPos != OpField) {
	++fieldPos;
    }

    if (fieldPos == LabelField && is_space(ch)) {
	tokenValue_clear(&tokenValue_);
	tokenKind = EMPTY_LABEL;
	return tokenKind;
    }

    // skip whitespace
    tokenValue_disable(&tokenValue_);
    while (is_space(ch) || ch == 0) {
	nextch();
    }
    tokenValue_clear(&tokenValue_);
    tokenValue_clear(&processedTokenValue_);
    tokenLoc.begin = tokenLoc.end;

    // check for identifiers
    if (is_letter(ch)) {
    	do {
	    nextch();
	} while (is_letter(ch) || is_digit(ch));
	tokenKind = IDENT;
	return tokenKind;
    }
    // check for numeral literals
    else if (is_digit(ch)) {
	if (ch == '0') {
	    // representation?
	    nextch();
	    if (ch == 'x' || ch == 'X') {
		tokenKind = HEXADECIMAL_LITERAL;
		nextch();
		if (!is_hexdigit(ch)) {
		    lexError(INVALID_HEXADECIMAL_LITERAL);
		}
		do {
		    nextch();
		} while (is_hexdigit(ch));
	    } else {
		tokenKind = OCTAL_LITERAL;
		while (is_octaldigit(ch)) {
		    nextch();
		}
	    }
	} else {
	    tokenKind = DECIMAL_LITERAL;
	    do {
		nextch();
	    } while (is_digit(ch));
	}
	return tokenKind;
    }
    // check for character literals
    else if (ch == '\'') {
	parseCharacterLiteral();
	tokenKind = CHARACTER_LITERAL;
	return tokenKind;
    }
    // check for string literals
    else if (ch == '"') {
	parseStringLiteral();
	tokenKind = STRING_LITERAL;
	return tokenKind;
    }
    // check for punctuators and @w[0-3] operator
    else if (ch == '\n') {
	nextch();
	fieldPos = EolField;
	if (tokenKind == EOL) {
	    // ignore empty lines
	    return getToken_();
	}
	tokenKind = EOL;
	return tokenKind;
    } else if (ch == '@') {
	nextch();
	if (ch != 'w') {
	    lexError(BAD_TOKEN);
	    tokenKind = UNKNOWN_TOKEN;
	    return tokenKind;
	} 
	nextch();
	if (ch == '0') {
	    nextch();
	    tokenKind = W0;
	    return tokenKind;
	} else if (ch == '1') {
	    nextch();
	    tokenKind = W1;
	    return tokenKind;
	} else if (ch == '2') {
	    nextch();
	    tokenKind = W2;
	    return tokenKind;
	} else if (ch == '3') {
	    nextch();
	    tokenKind = W3;
	    return tokenKind;
	}
	lexError(BAD_TOKEN);
	tokenKind = UNKNOWN_TOKEN;
	return tokenKind;
    } else if (ch == ':') {
	nextch();
	tokenKind = COLON;
	return tokenKind;
    } else if (ch == '+') {
	nextch();
	tokenKind = PLUS;
	return tokenKind;
    } else if (ch == '-') {
	nextch();
	tokenKind = MINUS;
	return tokenKind;
    } else if (ch == '*') {
	nextch();
	tokenKind = ASTERISK;
	return tokenKind;
    } else if (ch == '#') {
	// ignore single line comment
	do {
	    nextch();
	} while (ch != EOF && ch != '\n');
	return getToken_();
    } else if (ch == '/') {
	nextch();
	if (ch == '/') {
	    // ignore single line comment
	    do {
		nextch();
	    } while (ch != EOF && ch != '\n');
	    return getToken_();
	} else if (ch == '*') {
	    nextch();
	    bool star = false;
	    while (ch != EOF && (!star || ch != '/')) {
		star = ch == '*';
		nextch();
	    }
	    if (ch == EOF) {
		lexError(EOF_IN_DELIMITED_COMMENT);
	    }
	    nextch();
	    return getToken_();
	}
	tokenKind = SLASH;
	return tokenKind;
    } else if (ch == '$') {
	nextch();
	tokenKind = DOLLAR;
	return tokenKind;
    } else if (ch == '%') {
	nextch();
	tokenKind = PERCENT;
	return tokenKind;
    } else if (ch == ',') {
	nextch();
	tokenKind = COMMA;
	return tokenKind;
    } else if (ch == '(') {
	do {
	    nextch();
	} while (is_space(ch) && ch != EOF);
	if (ch == '%') {
	    nextch();
	    tokenKind = LPAREN_MEM;
	    return tokenKind;
	}
	tokenKind = LPAREN;
	return tokenKind;
    } else if (ch == ')') {
	nextch();
	tokenKind = RPAREN;
	return tokenKind;
    }
    // if we get here no legal token was found or end of file was reached
    else if (ch != EOF) {
	nextch();
	tokenKind = EOI;
	lexError(BAD_TOKEN);
	return tokenKind;
    }
    tokenKind = EOI;
    return tokenKind;
}

enum TokenKind
getToken()
{
    getToken_();

    if (tokenKind != EOI) {
	if (fieldPos == LabelField) {
	    if (tokenKind != IDENT && tokenKind != EMPTY_LABEL) {
		lexError(EXPECTED_LABEL);
	    }
	} else if (fieldPos == MnemonicField) {
	    if (tokenKind != COLON && ! tokenFixIdentToMnemomic()) {
		lexError(EXPECTED_MNEMONIC);
	    } else if (tokenKind == COLON) {
		fieldPos = LabelField;
	    }
	}
    }

    // printf("getToken: %d\n", tokenKind);
    return tokenKind;
}

void
setLexerIn(FILE *in)
{
    in_ = in;
}
