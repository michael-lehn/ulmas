#include <stdio.h>

#include "string.h"

int
main()
{
    const char *s1 = addString("prod");
    const char *s2 = addString("prodi");
    const char *s3 = addString("prod");
    const char *s4 = addString("prod");
    const char *s5 = addString("prodi");

    printf("s1 = %p -> %s\n", s1, s1);
    printf("s2 = %p -> %s\n", s2, s2);
    printf("s3 = %p -> %s\n", s3, s3);
    printf("s4 = %p -> %s\n", s4, s4);
    printf("s5 = %p -> %s\n", s5, s5);

    printStringPool();
}
