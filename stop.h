#ifndef ULMAS_STOP_H
#define ULMAS_STOP_H

#include <stdlib.h>
#include <stdio.h>

void deleteOutputOnStop(FILE *out, const char *outputfile);

void stop(int exitCode);

#endif // ULMAS_STOP_H
