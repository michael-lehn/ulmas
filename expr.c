#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "expr.h"
#include "instr.h"
#include "memregion.h"
#include "msg.h"
#include "stop.h"
#include "symtab.h"

static void
combineError(enum ExprOp op, const struct Expr *left,
	     const struct Expr *right)
{
    fprintf(stderr, "can not combine '");
    fprintExpr(stderr, left);
    fprintf(stderr, "' and '");
    fprintExpr(stderr, right);
    fprintf(stderr, "' with op=%di\n", op);
    stop(1);
}


static enum ExprType
combineExprTypes(enum ExprOp op, const struct Expr *left,
		 const struct Expr *right)
{
    enum ExprType t1 = left->type;
    enum ExprType t2 = right->type;

    if (t2 == ABSOLUTE) {
	enum ExprType tmp = t1;
	t1 = t2;
	t2 = tmp;
    }

    switch (op) {
	case ADD:
	case SUB:
	    if (t1 == ABSOLUTE && t2 == ABSOLUTE) {
		return ABSOLUTE;
	    } else if (t1 == ABSOLUTE && t2 != UNKNOWN) {
		return t2;
	    } else if (t1 != UNKNOWN && t1 == t2) {
		return ABSOLUTE;
	    }
	    return UNKNOWN;

	case MUL:
	case DIV:
	case MOD:
	    if (t1 == ABSOLUTE && t2 == ABSOLUTE) {
		return ABSOLUTE;
	    } else if (t1 == ABSOLUTE && t2 == UNKNOWN) {
		return UNKNOWN;
	    }
	    combineError(op, left, right);
	    stop(1);
	    return UNKNOWN;

	default:
	    combineError(op, left, right);
	    fprintf(stderr, "internal error. Function 'combineExprTypes'"
		    " called for op = %d\n", op);
	    stop(2);
	    return UNKNOWN;

    }
}

struct Expr *
makeValExpr(const struct TokenLoc *loc, enum ExprType type, uint64_t val,
	    const char *repr)
{
    struct Expr *expr = alloc(EXPR, sizeof(*expr));
    struct TokenLoc loc_ = {{ 0, 0}, {0, 0}};
    expr->op = VAL;
    expr->type = type;
    expr->loc = loc ? *loc : loc_;
    expr->u.lit.val = val;
    expr->u.lit.repr = repr;
    return expr;
}

struct Expr *
makeIdentExpr(const struct TokenLoc *loc, const char *ident)
{
    struct Expr *expr = alloc(DATA, sizeof(*expr));

    expr->op = SYM;
    expr->loc = *loc;
    expr->type = getSymType(ident);

    expr->u.sym = ident;
    return expr;
}

struct Expr *
makeBinaryExpr(enum ExprOp op, struct Expr *left,
	       struct Expr *right)
{
    struct Expr *expr = alloc(DATA, sizeof(*expr));
    expr->op = op;
    expr->type = combineExprTypes(op, left, right);
    expr->loc.begin = left->loc.begin;
    expr->loc.end = right->loc.end;
    expr->u.child[0] = left;
    expr->u.child[1] = right;
    return expr;
}

struct Expr *
makeUnaryExpr(const struct TokenLoc *loc, enum ExprOp op,
	      struct Expr *operand)
{
    struct Expr *expr = alloc(DATA, sizeof(*expr));
    expr->loc.begin = loc->begin;
    expr->loc.end = operand->loc.end;
    expr->op = op;
    expr->type = operand->type;
    expr->u.child[0] = operand;
    expr->u.child[1] = 0;
    return expr;
}


static const char *opStr[] = {
    "[val]",
    "[sym]",
    "[unary] -",
    "@w0",
    "@w1",
    "@w2",
    "@w3",
    "+",
    "-",
    "*",
    "/",
    "%",
};

static void
printExprWithIndent(const struct Expr *expr, size_t indent)
{
    for (size_t i=0; i<indent; ++i) {
	printf(" ");
    }
    printf("%s ", opStr[expr->op]);
    indent += 4;
    switch (expr->op) {
	case VAL:
	    printf("%s\n", expr->u.lit.repr);
	    break;
	case NEG:
	case OP_W0 ... OP_W3:
	    printf("\n");
	    printExprWithIndent(expr->u.child[0], indent);
	    break;
	default:
	    printf("\n");
	    printExprWithIndent(expr->u.child[0], indent);
	    printExprWithIndent(expr->u.child[1], indent);
    }
}

void
printExprTree(const struct Expr *expr)
{
    printExprWithIndent(expr, 0);
}

static void
fprintExpr_(FILE *out, const struct Expr *expr, bool isFactor)
{
    switch (expr->op) {
	case SYM:
	    fprintf(out, "%s", expr->u.sym);
	    break;

	case VAL:
	    fprintf(out, "%s", expr->u.lit.repr);
	    break;

	case OP_W0:
	    fprintf(out, "@w0(");
	    fprintExpr_(out, expr->u.child[0], true);
	    fprintf(out, ")");
	    break;

	case OP_W1:
	    fprintf(out, "@w1(");
	    fprintExpr_(out, expr->u.child[0], true);
	    fprintf(out, ")");
	    break;

	case OP_W2:
	    fprintf(out, "@w2(");
	    fprintExpr_(out, expr->u.child[0], true);
	    fprintf(out, ")");
	    break;

	case OP_W3:
	    fprintf(out, "@w3(");
	    fprintExpr_(out, expr->u.child[0], true);
	    fprintf(out, ")");
	    break;

	case NEG:
	    fprintf(out, "-");
	    fprintExpr_(out, expr->u.child[0], true);
	    break;

	case ADD:
	case SUB:
	    if (isFactor) {
		fprintf(out, "(");
	    }
	    fprintExpr_(out, expr->u.child[0], false);
	    fprintf(out, " %s ", opStr[expr->op]);
	    fprintExpr_(out, expr->u.child[1], false);
	    if (isFactor) {
		fprintf(out, ")");
	    }
	    break;

	default:
	    fprintExpr_(out, expr->u.child[0], true);
	    fprintf(out, " %s ", opStr[expr->op]);
	    fprintExpr_(out, expr->u.child[1], true);
    }
}

void
fprintExpr(FILE *out, const struct Expr *expr)
{
    fprintExpr_(out, expr, false);
}

size_t
fixExpressionType(struct Expr *expr)
{
    if (expr->type != UNKNOWN) {
	return 0;
    }

    // fix types
    switch (expr->op) {
	case VAL:
	    break;

	case SYM:
	    expr->type = getSymType(expr->u.sym);
	    break;

	case NEG:
	case OP_W0:
	case OP_W1:
	case OP_W2:
	case OP_W3:
	    fixExpressionType(expr->u.child[0]);
	    expr->type = expr->u.child[0]->type;
	    break;

	default:
	    fixExpressionType(expr->u.child[0]);
	    fixExpressionType(expr->u.child[1]);
	    expr->type = combineExprTypes(expr->op, expr->u.child[0],
					  expr->u.child[1]);
    }
    if (expr->type == UNKNOWN) {
	return 0;
    }
    return 1;
}

static uint64_t
evalExpr(const struct Expr *expr, const struct Expr *root)
{
    if (root == expr) {
	errorMsg(&root->loc, "reference cycle in expression");
	stop(1);
    }
    if (! root) {
	root = expr;
    }

    uint64_t result = 0;
    switch (expr->op) {
	case SYM:
	    if (!getSym(expr->u.sym)) {
		fprintf(stderr, "internal error");
		stop(2);
	    }
	    result = evalExpr(getSym(expr->u.sym), root);
	    if (expr->type < ABSOLUTE) {
		result += getSegmentOffset(expr->type);
	    }
	    break;

	case VAL:
	    result = expr->u.lit.val;
	    break;

	case OP_W0:
	    result = 0xFFFF & evalExpr(expr->u.child[0], root);
	    break;

	case OP_W1:
	    result = 0xFFFF & (evalExpr(expr->u.child[0], root) >> 16);
	    break;

	case OP_W2:
	    result = 0xFFFF & (evalExpr(expr->u.child[0], root) >> 32);
	    break;

	case OP_W3:
	    result = 0xFFFF & (evalExpr(expr->u.child[0], root) >> 48);
	    break;

	case NEG:
	    result = -evalExpr(expr->u.child[0], root);
	    break;

	case ADD:
	    result = evalExpr(expr->u.child[0], root);
	    result += evalExpr(expr->u.child[1], root);
	    break;

	case SUB:
	    result = evalExpr(expr->u.child[0], root);
	    result -= evalExpr(expr->u.child[1], root);
	    break;

	case MUL:
	    result = evalExpr(expr->u.child[0], root);
	    result *= evalExpr(expr->u.child[1], root);
	    break;

	case DIV:
	    result = evalExpr(expr->u.child[0], root);
	    result /= evalExpr(expr->u.child[1], root);
	    break;

	case MOD:
	    result = evalExpr(expr->u.child[0], root);
	    result %= evalExpr(expr->u.child[1], root);
	    break;
    }
    return result;
}

uint64_t
evalExpression(const struct Expr *expr)
{
    return evalExpr(expr, 0);
}

