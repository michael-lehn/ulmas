#ifndef ULMAS_PARSER_H
#define ULMAS_PARSER_H 1

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

extern size_t errorCount;
extern double result;

bool parse(FILE *in);

#endif // ULMAS_PARSER_H
