    ldzwq   @w3(foo),	%1
    shldwq  @w2(foo),	%1
    shldwq  @w1(foo),	%1
    shldwq  @w0(foo),	%1

    .set    foo,    0x123456789ABCDEF0

    ldzwq   @w3(foo2),	%1
    shldwq  @w2(foo2),	%1
    shldwq  @w1(foo2),	%1
    shldwq  @w0(foo2),	%1

