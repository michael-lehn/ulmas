    .data
    .byte   foo
    .byte   bar

    .text
    addq    foo, %2, %3
    addq    bar, %2, %3
    jmp	    foo+3
    .set    bar, 2
