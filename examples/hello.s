	.data
msg	.string     "hello \xF0\x9F\x98\x81\n"

	.text
	ldzwq       msg,     %1
check	movzbq      0(%1),  %2
	subq        0,      %2,     %0
	jz          halt
	putc        %2
	addq        1,      %1,     %1
	jmp         check
halt    halt        0
