    addq	1,	%2,	%3
    addq	%1,	%2,	%3

    andq	%1,	%2,	%3

    divq	1,	%2,	%3
    divq	%1,	%2,	%3

    getc	%1

    halt	1
    halt	%1

    idivq	1,	%2,	%3
    idivq	%1,	%2,	%3

    imulq	1,	%2,	%3
    imulq	%1,	%2,	%3

    ja		4
    ja		-4

    jae		4
    jae		-4

    jb		4
    jb		-4

    jbe		4
    jbe		-4

    je		4
    je		-4

    jg		4
    jg		-4

    jge		4
    jge		-4

    jl		4
    jl		-4

    jle		4
    jle		-4


    jmp		4
    jmp		-4
    jmp		%1,	%2

    jna		4
    jna		-4

    jnae	4
    jnae	-4

    jnb		4
    jnb		-4

    jnbe	4
    jnbe	-4

    jne		4
    jne		-4

    jng		4
    jng		-4

    jnge	4
    jnge	-4

    jnl		4
    jnl		-4

    jnle	4
    jnle	-4

    jnz		4
    jnz		-4

    jz		4
    jz		-4

    ldswq	1234,	%1
    ldswq	-1234,	%1

    ldzwq	1234,	%1

    movb	%3,	1(%2)
    movb	%3,	(%1, %2)

    movl	%3,	1(%2)
    movl	%3,	(%1, %2)

    movq	%3,	1(%2)
    movq	%3,	(%1, %2)

    movq	1(%2),	%3
    movq	(%1, %2),	%3

    movsbq	1(%2),	%3
    movsbq	(%1, %2),	%3

    movslq	1(%2),	%3
    movslq	(%1, %2),	%3

    movswq	1(%2),	%3
    movswq	(%1, %2),	%3

    movw	%3,	1(%2)
    movw	%3,	(%1, %2)

    movzbq	1(%2),	%3
    movzbq	(%1, %2),	%3

    movzlq	1(%2),	%3
    movzlq	(%1, %2),	%3

    movzwq	1(%2),	%3
    movzwq	(%1, %2),	%3

    mulq	1,	%2,	%3
    mulq	%1,	%2,	%3

    nop

    notq	%1,	%2

    orq		%1,	%2,	%3

    putc	1
    putc	%1

    salq	1,	%2,	%3
    salq	%1,	%2,	%3

    sarq	1,	%2,	%3
    sarq	%1,	%2,	%3

    shldwq	1234,	%2

    shlq	1,	%2,	%3
    shlq	%1,	%2,	%3

    shrq	1,	%2,	%3
    shrq	%1,	%2,	%3

    subq	1,	%2,	%3
    subq	%1,	%2,	%3

    trap	%1,	%2,	%3
