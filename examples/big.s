	.text
	.globl abs
# function header of int32_t abs(int32_t value):
abs:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of abs:
	# if (value < 0) { ... } else ...
	# condition: value < 0
	movslq 24(%1),%4
	subq $0,%4,%0
	jge .L2
.L1:
	movslq 24(%1),%5
	subq %5,%0,%4
	movl %4,16(%1)
	jmp .L0
	jmp .L3
.L2:
	movslq 24(%1),%4
	movl %4,16(%1)
	jmp .L0
.L3:
# ... } // if (value < 0)
	
# return from function abs:
.L0:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Enter
# function header of void Enter(int32_t t, int32_t value):
Enter:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of Enter:
	# *(*(hist_ring + t) + hist_write) = value;
	ldzwq $20,%4
	movslq 16(%1),%5
	imulq %5,%4,%5
	ldzwq $hist_ring,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $hist_write,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	movslq 20(%1),%4
	movl %4,0(%5)
# return from function Enter:
.L4:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl NewPeriod
# function header of void NewPeriod():
NewPeriod:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of NewPeriod:
	# hist_write = (hist_write + 1) % hist_length;
	ldzwq $hist_write,%5
	movslq 0(%5),%5
	addq $1,%5,%4
	ldzwq $hist_length,%7
	movslq 0(%7),%7
	idivq %7,%4,%5
	ldzwq $hist_write,%4
	movl %6,0(%4)
	# if (hist_written < hist_length) { ...
	# condition: hist_written < hist_length
	ldzwq $hist_written,%4
	movslq 0(%4),%4
	ldzwq $hist_length,%5
	movslq 0(%5),%5
	subq %5,%4,%0
	jge .L6
.L7:
# hist_written = hist_written + 1;
	ldzwq $hist_written,%5
	movslq 0(%5),%5
	addq $1,%5,%4
	ldzwq $hist_written,%5
	movl %4,0(%5)
.L6:
# ... } // if (hist_written < hist_length)
	
# return from function NewPeriod:
.L5:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Get
# function header of int32_t Get(int32_t t, int32_t age):
Get:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
	subq $8,%2,%2
# function body of Get:
	# if (age > hist_written) { ...
	# condition: age > hist_written
	movslq 28(%1),%4
	ldzwq $hist_written,%5
	movslq 0(%5),%5
	subq %5,%4,%0
	jle .L9
.L10:
	ldswq $-1,%4
	movl %4,16(%1)
	jmp .L8
.L9:
# ... } // if (age > hist_written)
	ldzwq $hist_write,%5
	movslq 0(%5),%5
	ldzwq $hist_length,%6
	movslq 0(%6),%6
	addq %5,%6,%4
	movslq 28(%1),%6
	subq %6,%4,%5
	ldzwq $hist_length,%4
	movslq 0(%4),%4
	idivq %4,%5,%6
	# initializer assigns (hist_write + hist_length - age) % hist_length to index + 0
	movl %7,-4(%1)
	ldzwq $20,%4
	movslq 24(%1),%5
	imulq %5,%4,%5
	ldzwq $hist_ring,%6
	addq %6,%5,%4
	ldzwq $4,%5
	movslq -4(%1),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	movslq 0(%5),%5
	movl %5,16(%1)
# return from function Get:
.L8:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl ReadInt
# function header of void ReadInt(int32_t *value):
ReadInt:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of ReadInt:
	# if (!geti(10, value)) { ...
	# condition: !geti(10, value)
	# function call: geti(10, value)
	subq $40,%2,%2
	ldzwq $10,%4
	movl %4,24(%2)
	movq 16(%1),%4
	movq %4,32(%2)
	ldzwq $geti,%4
	jmp %4,%3
	movzbq 16(%2),%4
	addq $40,%2,%2
	subq %4,%0,%0
	jne .L12
.L13:
# puts("Hatte eine ganze Zahl erwartet. Tschüss!\n");
	# function call: puts("Hatte eine ganze Zahl erwartet. Tschüss!\n")
	subq $24,%2,%2
	ldzwq $.L14,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# exit(1);
	# function call: exit(1)
	subq $24,%2,%2
	ldzwq $1,%4
	movl %4,16(%2)
	ldzwq $exit,%4
	jmp %4,%3
	addq $24,%2,%2
.L12:
# ... } // if (!geti(10, value))
	
# return from function ReadInt:
.L11:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl WriteInt
# function header of void WriteInt(int32_t value):
WriteInt:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of WriteInt:
	# puti(value, 0);
	# function call: puti(value, 0)
	subq $32,%2,%2
	movslq 16(%1),%4
	movq %4,16(%2)
	ldzwq $0,%4
	movq %4,24(%2)
	ldzwq $puti,%4
	jmp %4,%3
	addq $32,%2,%2
# return from function WriteInt:
.L15:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl ReadAgain
# function header of void ReadAgain():
ReadAgain:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of ReadAgain:
	# inbuf_back(&stdin);
	# function call: inbuf_back(&stdin)
	subq $32,%2,%2
	ldzwq $stdin,%4
	movq %4,24(%2)
	ldzwq $inbuf_back,%4
	jmp %4,%3
	movzbq 16(%2),%4
	addq $32,%2,%2
# return from function ReadAgain:
.L16:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Random
# function header of int32_t Random(int32_t lower, int32_t upper):
Random:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of Random:
	# function call: rand()
	subq $24,%2,%2
	ldzwq $rand,%4
	jmp %4,%3
	movzlq 16(%2),%4
	addq $24,%2,%2
	movslq 28(%1),%6
	movslq 24(%1),%7
	subq %7,%6,%5
	addq $1,%5,%6
	addq $0,%4,%10
	addq $0,%0,%11
	divq %6,%10,%7
	movslq 24(%1),%5
	addq %5,%9,%4
	movl %4,16(%1)
# return from function Random:
.L17:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Actions
# function header of int32_t Actions():
Actions:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of Actions:
	ldzwq $116,%4
	ldzwq $schwarz,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $produktion,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $prodi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $116,%4
	ldzwq $schwarz,%6
	movslq 0(%6),%6
	imulq %6,%4,%6
	ldzwq $lebensqualitaet,%7
	addq %7,%6,%4
	ldzwq $4,%6
	ldzwq $lebi,%7
	movslq 0(%7),%7
	imulq %7,%6,%7
	addq %4,%7,%6
	movslq 0(%5),%5
	movslq 0(%6),%6
	addq %5,%6,%4
	ldzwq $192,%5
	ldzwq $schwarz,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	ldzwq $bevoelkerung,%7
	addq %7,%6,%5
	ldzwq $4,%6
	ldzwq $bevi,%7
	movslq 0(%7),%7
	imulq %7,%6,%7
	addq %5,%7,%6
	movslq 0(%6),%6
	addq %4,%6,%5
	ldzwq $192,%4
	ldzwq $schwarz,%6
	movslq 0(%6),%6
	imulq %6,%4,%6
	ldzwq $politik,%7
	addq %7,%6,%4
	ldzwq $4,%6
	ldzwq $poli,%7
	movslq 0(%7),%7
	imulq %7,%6,%7
	addq %4,%7,%6
	movslq 0(%6),%6
	addq %5,%6,%4
	movl %4,16(%1)
# return from function Actions:
.L18:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Range
# function header of void Range(int32_t *index, int32_t high):
Range:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of Range:
	# if (*index < 0) { ... } else ...
	# condition: *index < 0
	movq 16(%1),%4
	movslq 0(%4),%4
	subq $0,%4,%0
	jge .L21
.L20:
# *index = 0;
	movq 16(%1),%4
	ldzwq $0,%5
	movl %5,0(%4)
	jmp .L22
.L21:
# if (*index > high) { ...
	# condition: *index > high
	movq 16(%1),%4
	movslq 0(%4),%4
	movslq 24(%1),%5
	subq %5,%4,%0
	jle .L23
.L24:
# *index = high;
	movq 16(%1),%4
	movslq 24(%1),%5
	movl %5,0(%4)
.L23:
# ... } // if (*index > high)
	
.L22:
# ... } // if (*index < 0)
	
# return from function Range:
.L19:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Alter
# function header of int32_t Alter(int32_t t, int32_t value):
Alter:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of Alter:
	# if (t == pol) { ... } else ...
	# condition: t == pol
	movslq 24(%1),%4
	ldzwq $pol,%5
	movslq 0(%5),%5
	subq %5,%4,%0
	jne .L27
.L26:
	movslq 28(%1),%5
	addq $40,%5,%4
	movl %4,16(%1)
	jmp .L25
	jmp .L28
.L27:
# if (t == leb) { ... } else ...
	# condition: t == leb
	movslq 24(%1),%4
	ldzwq $leb,%5
	movslq 0(%5),%5
	subq %5,%4,%0
	jne .L30
.L29:
	movslq 28(%1),%5
	subq $1,%5,%4
	movl %4,16(%1)
	jmp .L25
	jmp .L31
.L30:
# if (t == umw) { ... } else ...
	# condition: t == umw
	movslq 24(%1),%4
	ldzwq $umw,%5
	movslq 0(%5),%5
	subq %5,%4,%0
	jne .L33
.L32:
	ldzwq $2,%5
	movslq 28(%1),%6
	imulq %5,%6,%4
	ldzwq $56,%6
	subq %4,%6,%5
	movl %5,16(%1)
	jmp .L25
	jmp .L34
.L33:
# if (t == prod) { ... } else ...
	# condition: t == prod
	movslq 24(%1),%4
	ldzwq $prod,%5
	movslq 0(%5),%5
	subq %5,%4,%0
	jne .L36
.L35:
	ldzwq $2,%5
	movslq 28(%1),%6
	imulq %5,%6,%4
	movl %4,16(%1)
	jmp .L25
	jmp .L37
.L36:
# if (t == bev) { ...
	# condition: t == bev
	movslq 24(%1),%4
	ldzwq $bev,%5
	movslq 0(%5),%5
	subq %5,%4,%0
	jne .L38
.L39:
	movslq 28(%1),%5
	addq $1,%5,%4
	idivq $2,%4,%5
	movl %5,16(%1)
	jmp .L25
.L38:
# ... } // if (t == bev)
	
.L37:
# ... } // if (t == prod)
	
.L34:
# ... } // if (t == umw)
	
.L31:
# ... } // if (t == leb)
	
.L28:
# ... } // if (t == pol)
	
# return from function Alter:
.L25:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Frage
# function header of int32_t Frage(int32_t plusminus):
Frage:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
	subq $8,%2,%2
# function body of Frage:
	# puts("Gewünschte Bevölkerungszunahme im Bereich von ");
	# function call: puts("Gewünschte Bevölkerungszunahme im Bereich von ")
	subq $24,%2,%2
	ldzwq $.L41,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# WriteInt(-plusminus);
	# function call: WriteInt(-plusminus)
	subq $24,%2,%2
	movslq 24(%1),%5
	subq %5,%0,%4
	movl %4,16(%2)
	ldzwq $WriteInt,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts(" bis ");
	# function call: puts(" bis ")
	subq $24,%2,%2
	ldzwq $.L42,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# WriteInt(plusminus);
	# function call: WriteInt(plusminus)
	subq $24,%2,%2
	movslq 24(%1),%4
	movl %4,16(%2)
	ldzwq $WriteInt,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts(": ");
	# function call: puts(": ")
	subq $24,%2,%2
	ldzwq $.L43,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# while (true) { ...
	jmp .L44
.L45:
# ReadInt(&antwort);
	# function call: ReadInt(&antwort)
	subq $24,%2,%2
	subq $4,%1,%4
	movq %4,16(%2)
	ldzwq $ReadInt,%4
	jmp %4,%3
	addq $24,%2,%2
	# if (antwort >= -plusminus && antwort <= plusminus) { ...
	# condition: antwort >= -plusminus && antwort <= plusminus
	movslq 24(%1),%5
	subq %5,%0,%4
	movslq -4(%1),%5
	subq %4,%5,%0
	jl .L47
	movslq -4(%1),%4
	movslq 24(%1),%5
	subq %5,%4,%0
	jg .L47
.L48:
	movslq -4(%1),%4
	movl %4,16(%1)
	jmp .L40
.L47:
# ... } // if (antwort >= -plusminus && antwort <= plusminus)
	# condition: true
.L44:
	ldzwq $1,%4
	subq %0,%4,%0
	jne .L45
.L46:
# ... } // while (true)
	
# return from function Frage:
.L40:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Zyklus
# function header of void Zyklus():
Zyklus:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of Zyklus:
	# puts("\n");
	# function call: puts("\n")
	subq $24,%2,%2
	ldzwq $.L51,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# sani = sani - Random(0, 1);
	# function call: Random(0, 1)
	subq $32,%2,%2
	ldzwq $0,%4
	movl %4,24(%2)
	ldzwq $1,%4
	movl %4,28(%2)
	ldzwq $Random,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	ldzwq $sani,%6
	movslq 0(%6),%6
	subq %4,%6,%5
	ldzwq $sani,%4
	movl %5,0(%4)
	# Range(&sani, 28);
	# function call: Range(&sani, 28)
	subq $32,%2,%2
	ldzwq $sani,%4
	movq %4,16(%2)
	ldzwq $28,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# umwi = umwi - *(*(sanierung + blau) + sani);
	ldzwq $116,%4
	ldzwq $blau,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $sanierung,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $sani,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $umwi,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	subq %5,%6,%4
	ldzwq $umwi,%5
	movl %4,0(%5)
	# prodi = prodi + *(*(produktion + rot) + prodi);
	ldzwq $116,%4
	ldzwq $rot,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $produktion,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $prodi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $prodi,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	addq %6,%5,%4
	ldzwq $prodi,%5
	movl %4,0(%5)
	# Range(&prodi, 28);
	# function call: Range(&prodi, 28)
	subq $32,%2,%2
	ldzwq $prodi,%4
	movq %4,16(%2)
	ldzwq $28,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# umwi = umwi + *(*(produktion + blau) + prodi);
	ldzwq $116,%4
	ldzwq $blau,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $produktion,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $prodi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $umwi,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	addq %6,%5,%4
	ldzwq $umwi,%5
	movl %4,0(%5)
	# Range(&umwi, 28);
	# function call: Range(&umwi, 28)
	subq $32,%2,%2
	ldzwq $umwi,%4
	movq %4,16(%2)
	ldzwq $28,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# umwi = umwi - *(*(umweltbelastung + rot) + umwi);
	ldzwq $116,%4
	ldzwq $rot,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $umweltbelastung,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $umwi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $umwi,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	subq %5,%6,%4
	ldzwq $umwi,%5
	movl %4,0(%5)
	# Range(&umwi, 28);
	# function call: Range(&umwi, 28)
	subq $32,%2,%2
	ldzwq $umwi,%4
	movq %4,16(%2)
	ldzwq $28,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# lebi = lebi - *(*(umweltbelastung + blau) + umwi);
	ldzwq $116,%4
	ldzwq $blau,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $umweltbelastung,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $umwi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $lebi,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	subq %5,%6,%4
	ldzwq $lebi,%5
	movl %4,0(%5)
	# if (Alter(prod, prodi) + 1 < Alter(bev, bevi)) { ... } else ...
	# condition: Alter(prod, prodi) + 1 < Alter(bev, bevi)
	# function call: Alter(prod, prodi)
	subq $32,%2,%2
	ldzwq $prod,%4
	movslq 0(%4),%4
	movl %4,24(%2)
	ldzwq $prodi,%4
	movslq 0(%4),%4
	movl %4,28(%2)
	ldzwq $Alter,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	addq $1,%4,%5
	# function call: Alter(bev, bevi)
	subq $40,%2,%2
	ldzwq $bev,%4
	movslq 0(%4),%4
	movl %4,24(%2)
	ldzwq $bevi,%4
	movslq 0(%4),%4
	movl %4,28(%2)
	ldzwq $Alter,%4
	movq %5,32(%2)
	jmp %4,%3
	movq 32(%2),%5
	movslq 16(%2),%4
	addq $40,%2,%2
	subq %4,%5,%0
	jge .L53
.L52:
# lebi = lebi - (bevi - prodi) / 2;
	ldzwq $bevi,%5
	movslq 0(%5),%5
	ldzwq $prodi,%6
	movslq 0(%6),%6
	subq %6,%5,%4
	idivq $2,%4,%5
	ldzwq $lebi,%6
	movslq 0(%6),%6
	subq %5,%6,%4
	ldzwq $lebi,%5
	movl %4,0(%5)
	jmp .L54
.L53:
# if (Alter(prod, prodi) > Alter(bev, bevi)) { ...
	# condition: Alter(prod, prodi) > Alter(bev, bevi)
	# function call: Alter(prod, prodi)
	subq $32,%2,%2
	ldzwq $prod,%4
	movslq 0(%4),%4
	movl %4,24(%2)
	ldzwq $prodi,%4
	movslq 0(%4),%4
	movl %4,28(%2)
	ldzwq $Alter,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	# function call: Alter(bev, bevi)
	subq $40,%2,%2
	ldzwq $bev,%5
	movslq 0(%5),%5
	movl %5,24(%2)
	ldzwq $bevi,%5
	movslq 0(%5),%5
	movl %5,28(%2)
	ldzwq $Alter,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	subq %5,%4,%0
	jle .L55
.L56:
# lebi = lebi + 1;
	ldzwq $lebi,%5
	movslq 0(%5),%5
	addq $1,%5,%4
	ldzwq $lebi,%5
	movl %4,0(%5)
.L55:
# ... } // if (Alter(prod, prodi) > Alter(bev, bevi))
	
.L54:
# ... } // if (Alter(prod, prodi) + 1 < Alter(bev, bevi))
	# aufi = aufi + *(*(aufklaerung + rot) + aufi);
	ldzwq $116,%4
	ldzwq $rot,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $aufklaerung,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $aufi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $aufi,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	addq %6,%5,%4
	ldzwq $aufi,%5
	movl %4,0(%5)
	# Range(&aufi, 28);
	# function call: Range(&aufi, 28)
	subq $32,%2,%2
	ldzwq $aufi,%4
	movq %4,16(%2)
	ldzwq $28,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# lebi = lebi + *(*(aufklaerung + blau) + aufi);
	ldzwq $116,%4
	ldzwq $blau,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $aufklaerung,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $aufi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $lebi,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	addq %6,%5,%4
	ldzwq $lebi,%5
	movl %4,0(%5)
	# Range(&lebi, 28);
	# function call: Range(&lebi, 28)
	subq $32,%2,%2
	ldzwq $lebi,%4
	movq %4,16(%2)
	ldzwq $28,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# if (aufi >= 20) { ... } else ...
	# condition: aufi >= 20
	ldzwq $aufi,%4
	movslq 0(%4),%4
	subq $20,%4,%0
	jl .L58
.L57:
# veri = veri + Frage(*(*(aufklaerung + gruen) + aufi));
	# function call: Frage(*(*(aufklaerung + gruen) + aufi))
	subq $32,%2,%2
	ldzwq $116,%4
	ldzwq $gruen,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $aufklaerung,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $aufi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	movslq 0(%5),%5
	movl %5,24(%2)
	ldzwq $Frage,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	ldzwq $veri,%6
	movslq 0(%6),%6
	addq %6,%4,%5
	ldzwq $veri,%4
	movl %5,0(%4)
	jmp .L59
.L58:
# veri = veri + *(*(aufklaerung + gruen) + aufi);
	ldzwq $116,%4
	ldzwq $gruen,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $aufklaerung,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $aufi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $veri,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	addq %6,%5,%4
	ldzwq $veri,%5
	movl %4,0(%5)
.L59:
# ... } // if (aufi >= 20)
	# lebi = lebi + *(*(lebensqualitaet + rot) + lebi);
	ldzwq $116,%4
	ldzwq $rot,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $lebensqualitaet,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $lebi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $lebi,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	addq %6,%5,%4
	ldzwq $lebi,%5
	movl %4,0(%5)
	# Range(&lebi, 28);
	# function call: Range(&lebi, 28)
	subq $32,%2,%2
	ldzwq $lebi,%4
	movq %4,16(%2)
	ldzwq $28,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# veri = veri + *(*(lebensqualitaet + blau) + lebi);
	ldzwq $116,%4
	ldzwq $blau,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $lebensqualitaet,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $lebi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $veri,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	addq %6,%5,%4
	ldzwq $veri,%5
	movl %4,0(%5)
	# Range(&veri, 28);
	# function call: Range(&veri, 28)
	subq $32,%2,%2
	ldzwq $veri,%4
	movq %4,16(%2)
	ldzwq $28,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# bevi = bevi + *(*(vermehrungsrate + rot) + veri) * *(*(bevoelkerung + rot) + bevi) + Random(-1, 1);
	ldzwq $116,%4
	ldzwq $rot,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $vermehrungsrate,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $veri,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $192,%4
	ldzwq $rot,%6
	movslq 0(%6),%6
	imulq %6,%4,%6
	ldzwq $bevoelkerung,%7
	addq %7,%6,%4
	ldzwq $4,%6
	ldzwq $bevi,%7
	movslq 0(%7),%7
	imulq %7,%6,%7
	addq %4,%7,%6
	movslq 0(%5),%5
	movslq 0(%6),%6
	imulq %5,%6,%4
	ldzwq $bevi,%6
	movslq 0(%6),%6
	addq %6,%4,%5
	# function call: Random(-1, 1)
	subq $40,%2,%2
	ldswq $-1,%4
	movl %4,24(%2)
	ldzwq $1,%4
	movl %4,28(%2)
	ldzwq $Random,%4
	movq %5,32(%2)
	jmp %4,%3
	movq 32(%2),%5
	movslq 16(%2),%4
	addq $40,%2,%2
	addq %5,%4,%6
	ldzwq $bevi,%4
	movl %6,0(%4)
	# Range(&bevi, 47);
	# function call: Range(&bevi, 47)
	subq $32,%2,%2
	ldzwq $bevi,%4
	movq %4,16(%2)
	ldzwq $47,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# lebi = lebi - *(*(bevoelkerung + blau) + bevi);
	ldzwq $192,%4
	ldzwq $blau,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $bevoelkerung,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $bevi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $lebi,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	subq %5,%6,%4
	ldzwq $lebi,%5
	movl %4,0(%5)
	# Range(&lebi, 28);
	# function call: Range(&lebi, 28)
	subq $32,%2,%2
	ldzwq $lebi,%4
	movq %4,16(%2)
	ldzwq $28,%4
	movl %4,24(%2)
	ldzwq $Range,%4
	jmp %4,%3
	addq $32,%2,%2
	# poli = poli + *(*(lebensqualitaet + gruen) + lebi);
	ldzwq $116,%4
	ldzwq $gruen,%5
	movslq 0(%5),%5
	imulq %5,%4,%5
	ldzwq $lebensqualitaet,%6
	addq %6,%5,%4
	ldzwq $4,%5
	ldzwq $lebi,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	addq %4,%6,%5
	ldzwq $poli,%6
	movslq 0(%6),%6
	movslq 0(%5),%5
	addq %6,%5,%4
	ldzwq $poli,%5
	movl %4,0(%5)
# return from function Zyklus:
.L50:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl News
# function header of void News():
News:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of News:
	# puts("\nLetztes Wahlergebnis: ");
	# function call: puts("\nLetztes Wahlergebnis: ")
	subq $24,%2,%2
	ldzwq $.L61,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# WriteInt(Alter(pol, poli));
	# function call: WriteInt(Alter(pol, poli))
	subq $24,%2,%2
	# function call: Alter(pol, poli)
	subq $32,%2,%2
	ldzwq $pol,%4
	movslq 0(%4),%4
	movl %4,24(%2)
	ldzwq $poli,%4
	movslq 0(%4),%4
	movl %4,28(%2)
	ldzwq $Alter,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	movl %4,16(%2)
	ldzwq $WriteInt,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts("%\t               ");
	# function call: puts("%\t               ")
	subq $24,%2,%2
	ldzwq $.L62,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts("Bevölkerung in Mio.: ");
	# function call: puts("Bevölkerung in Mio.: ")
	subq $24,%2,%2
	ldzwq $.L63,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# WriteInt(Alter(bev, bevi));
	# function call: WriteInt(Alter(bev, bevi))
	subq $24,%2,%2
	# function call: Alter(bev, bevi)
	subq $32,%2,%2
	ldzwq $bev,%4
	movslq 0(%4),%4
	movl %4,24(%2)
	ldzwq $bevi,%4
	movslq 0(%4),%4
	movl %4,28(%2)
	ldzwq $Alter,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	movl %4,16(%2)
	ldzwq $WriteInt,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts("\nBruttosozialprodukt in Mrd.: ");
	# function call: puts("\nBruttosozialprodukt in Mrd.: ")
	subq $24,%2,%2
	ldzwq $.L64,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# WriteInt(Alter(prod, prodi));
	# function call: WriteInt(Alter(prod, prodi))
	subq $24,%2,%2
	# function call: Alter(prod, prodi)
	subq $32,%2,%2
	ldzwq $prod,%4
	movslq 0(%4),%4
	movl %4,24(%2)
	ldzwq $prodi,%4
	movslq 0(%4),%4
	movl %4,28(%2)
	ldzwq $Alter,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	movl %4,16(%2)
	ldzwq $WriteInt,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts("\t               ");
	# function call: puts("\t               ")
	subq $24,%2,%2
	ldzwq $.L65,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts("Zustandsindex der Umwelt: ");
	# function call: puts("Zustandsindex der Umwelt: ")
	subq $24,%2,%2
	ldzwq $.L66,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# WriteInt(Alter(umw, umwi));
	# function call: WriteInt(Alter(umw, umwi))
	subq $24,%2,%2
	# function call: Alter(umw, umwi)
	subq $32,%2,%2
	ldzwq $umw,%4
	movslq 0(%4),%4
	movl %4,24(%2)
	ldzwq $umwi,%4
	movslq 0(%4),%4
	movl %4,28(%2)
	ldzwq $Alter,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	movl %4,16(%2)
	ldzwq $WriteInt,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts("\nLebensqualitätsindex des Amts für Statistik: ");
	# function call: puts("\nLebensqualitätsindex des Amts für Statistik: ")
	subq $24,%2,%2
	ldzwq $.L67,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
	# WriteInt(Alter(leb, lebi));
	# function call: WriteInt(Alter(leb, lebi))
	subq $24,%2,%2
	# function call: Alter(leb, lebi)
	subq $32,%2,%2
	ldzwq $leb,%4
	movslq 0(%4),%4
	movl %4,24(%2)
	ldzwq $lebi,%4
	movslq 0(%4),%4
	movl %4,28(%2)
	ldzwq $Alter,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	movl %4,16(%2)
	ldzwq $WriteInt,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts("\n");
	# function call: puts("\n")
	subq $24,%2,%2
	ldzwq $.L68,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
# return from function News:
.L60:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl PrintHistory
# function header of void PrintHistory(int32_t t, int32_t age):
PrintHistory:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
	subq $8,%2,%2
# function body of PrintHistory:
	# for (int32_t a = age; a >= 0; a -= 1) { ...
	# initializer assigns age to a + 0
	movslq 20(%1),%4
	movl %4,-4(%1)
	jmp .L72
.L70:
# function call: Get(t, a)
	subq $32,%2,%2
	movslq 16(%1),%4
	movl %4,24(%2)
	movslq -4(%1),%4
	movl %4,28(%2)
	ldzwq $Get,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	# initializer assigns Get(t, a) to value + 0
	movl %4,-8(%1)
	# if (value >= 0) { ...
	# condition: value >= 0
	movslq -8(%1),%4
	subq $0,%4,%0
	jl .L74
.L75:
# WriteInt(Alter(t, value));
	# function call: WriteInt(Alter(t, value))
	subq $24,%2,%2
	# function call: Alter(t, value)
	subq $32,%2,%2
	movslq 16(%1),%4
	movl %4,24(%2)
	movslq -8(%1),%4
	movl %4,28(%2)
	ldzwq $Alter,%4
	jmp %4,%3
	movslq 16(%2),%4
	addq $32,%2,%2
	movl %4,16(%2)
	ldzwq $WriteInt,%4
	jmp %4,%3
	addq $24,%2,%2
	# puts(" ");
	# function call: puts(" ")
	subq $24,%2,%2
	ldzwq $.L76,%4
	movq %4,16(%2)
	ldzwq $puts,%4
	jmp %4,%3
	addq $24,%2,%2
.L74:
# ... } // if (value >= 0)
	# increment of for loop: a -= 1
.L71:
	ldzwq $1,%4
	subq $4,%1,%5
	movslq 0(%5),%6
	subq %4,%6,%4
	movl %4,0(%5)
	# condition: a >= 0
.L72:
	movslq -4(%1),%5
	subq $0,%5,%0
	jge .L70
.L73:
# ... } // for (int32_t a = age; a >= 0; a -= 1)
	# puts("\n");
	# function call: puts("\n")
	subq $32,%2,%2
	ldzwq $.L77,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
# return from function PrintHistory:
.L69:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Tuwat
# function header of void Tuwat():
Tuwat:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
	subq $8,%2,%2
# function body of Tuwat:
	# function call: Actions()
	subq $32,%2,%2
	ldzwq $Actions,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	# initializer assigns Actions() to i + 0
	movl %5,-4(%1)
	# if (i <= 6) { ... } else ...
	# condition: i <= 6
	movslq -4(%1),%5
	subq $6,%5,%0
	jg .L80
.L79:
	jmp .L81
.L80:
# if (i <= 12) { ... } else ...
	# condition: i <= 12
	movslq -4(%1),%5
	subq $12,%5,%0
	jg .L83
.L82:
# i = 7 + (i - 7) / 2;
	movslq -4(%1),%6
	subq $7,%6,%5
	idivq $2,%5,%6
	ldzwq $7,%7
	addq %7,%6,%5
	movl %5,-4(%1)
	jmp .L84
.L83:
# if (i <= 18) { ... } else ...
	# condition: i <= 18
	movslq -4(%1),%5
	subq $18,%5,%0
	jg .L86
.L85:
# i = 10 + (i - 13) / 3;
	movslq -4(%1),%6
	subq $13,%6,%5
	idivq $3,%5,%6
	ldzwq $10,%7
	addq %7,%6,%5
	movl %5,-4(%1)
	jmp .L87
.L86:
# if (i <= 22) { ... } else ...
	# condition: i <= 22
	movslq -4(%1),%5
	subq $22,%5,%0
	jg .L89
.L88:
# i = 12;
	ldzwq $12,%5
	movl %5,-4(%1)
	jmp .L90
.L89:
# if (i <= 27) { ... } else ...
	# condition: i <= 27
	movslq -4(%1),%5
	subq $27,%5,%0
	jg .L92
.L91:
# i = 13;
	ldzwq $13,%5
	movl %5,-4(%1)
	jmp .L93
.L92:
# if (i <= 33) { ... } else ...
	# condition: i <= 33
	movslq -4(%1),%5
	subq $33,%5,%0
	jg .L95
.L94:
# i = 14;
	ldzwq $14,%5
	movl %5,-4(%1)
	jmp .L96
.L95:
# i = 15;
	ldzwq $15,%5
	movl %5,-4(%1)
.L96:
# ... } // if (i <= 33)
	
.L93:
# ... } // if (i <= 27)
	
.L90:
# ... } // if (i <= 22)
	
.L87:
# ... } // if (i <= 18)
	
.L84:
# ... } // if (i <= 12)
	
.L81:
# ... } // if (i <= 6)
	# aktionen = aktionen + i;
	ldzwq $aktionen,%6
	movslq 0(%6),%6
	movslq -4(%1),%7
	addq %6,%7,%5
	ldzwq $aktionen,%6
	movl %5,0(%6)
	# ReadAgain();
	# function call: ReadAgain()
	subq $24,%2,%2
	ldzwq $ReadAgain,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	# initializer assigns getchar() to ch + 0
	movl %5,-8(%1)
	# while (ch != '\n') { ...
	jmp .L97
.L98:
# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-8(%1)
	# condition: ch != '\n'
.L97:
	movslq -8(%1),%5
	subq $10,%5,%0
	jne .L98
.L99:
# ... } // while (ch != '\n')
	# while (true) { ...
	jmp .L100
.L101:
# puts("Sie haben ");
	# function call: puts("Sie haben ")
	subq $32,%2,%2
	ldzwq $.L103,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# WriteInt(aktionen);
	# function call: WriteInt(aktionen)
	subq $32,%2,%2
	ldzwq $aktionen,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	ldzwq $WriteInt,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts(" Aktionsmöglichkeiten.\n");
	# function call: puts(" Aktionsmöglichkeiten.\n")
	subq $32,%2,%2
	ldzwq $.L104,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# if (aktionen > 0) { ... } else ...
	# condition: aktionen > 0
	ldzwq $aktionen,%5
	movslq 0(%5),%5
	subq $0,%5,%0
	jle .L106
.L105:
# puts("> ");
	# function call: puts("> ")
	subq $32,%2,%2
	ldzwq $.L108,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-8(%1)
	# while (ch == ' ') { ...
	jmp .L109
.L110:
# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-8(%1)
	# condition: ch == ' '
.L109:
	movslq -8(%1),%5
	subq $32,%5,%0
	je .L110
.L111:
# ... } // while (ch == ' ')
	# if (ch >= '0' && ch <= '9' || ch == '-') { ... } else ...
	# condition: ch >= '0' && ch <= '9' || ch == '-'
	movslq -8(%1),%5
	subq $48,%5,%0
	jl .L115
	movslq -8(%1),%5
	subq $57,%5,%0
	jle .L112
.L115:
	movslq -8(%1),%5
	subq $45,%5,%0
	jne .L113
.L112:
# ReadAgain();
	# function call: ReadAgain()
	subq $24,%2,%2
	ldzwq $ReadAgain,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# ReadInt(&i);
	# function call: ReadInt(&i)
	subq $32,%2,%2
	subq $4,%1,%5
	movq %5,16(%2)
	ldzwq $ReadInt,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-8(%1)
	# while (ch == ' ') { ...
	jmp .L117
.L118:
# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-8(%1)
	# condition: ch == ' '
.L117:
	movslq -8(%1),%5
	subq $32,%5,%0
	je .L118
.L119:
# ... } // while (ch == ' ')
	jmp .L114
.L113:
# i = 0;
	ldzwq $0,%5
	movl %5,-4(%1)
.L114:
# ... } // if (ch >= '0' && ch <= '9' || ch == '-')
	# if (abs(i) > aktionen && ch != 'h') { ... } else ...
	# condition: abs(i) > aktionen && ch != 'h'
	# function call: abs(i)
	subq $40,%2,%2
	movslq -4(%1),%5
	movl %5,24(%2)
	ldzwq $abs,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $aktionen,%6
	movslq 0(%6),%6
	subq %6,%5,%0
	jle .L121
	movslq -8(%1),%5
	subq $104,%5,%0
	je .L121
.L120:
# puts("Sie überschätzen Ihre Kräfte!\n");
	# function call: puts("Sie überschätzen Ihre Kräfte!\n")
	subq $32,%2,%2
	ldzwq $.L124,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L122
.L121:
# if (ch == '\n') { ... } else ...
	# condition: ch == '\n'
	movslq -8(%1),%5
	subq $10,%5,%0
	jne .L126
.L125:
	jmp .L127
.L126:
# if (ch == 's') { ... } else ...
	# condition: ch == 's'
	movslq -8(%1),%5
	subq $115,%5,%0
	jne .L129
.L128:
# sani = sani + i;
	ldzwq $sani,%6
	movslq 0(%6),%6
	movslq -4(%1),%7
	addq %6,%7,%5
	ldzwq $sani,%6
	movl %5,0(%6)
	# Range(&sani, 28);
	# function call: Range(&sani, 28)
	subq $40,%2,%2
	ldzwq $sani,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# aktionen = aktionen - abs(i);
	# function call: abs(i)
	subq $40,%2,%2
	movslq -4(%1),%5
	movl %5,24(%2)
	ldzwq $abs,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $aktionen,%7
	movslq 0(%7),%7
	subq %5,%7,%6
	ldzwq $aktionen,%5
	movl %6,0(%5)
	jmp .L130
.L129:
# if (ch == 'p') { ... } else ...
	# condition: ch == 'p'
	movslq -8(%1),%5
	subq $112,%5,%0
	jne .L132
.L131:
# prodi = prodi + i;
	ldzwq $prodi,%6
	movslq 0(%6),%6
	movslq -4(%1),%7
	addq %6,%7,%5
	ldzwq $prodi,%6
	movl %5,0(%6)
	# Range(&prodi, 28);
	# function call: Range(&prodi, 28)
	subq $40,%2,%2
	ldzwq $prodi,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# aktionen = aktionen - abs(i);
	# function call: abs(i)
	subq $40,%2,%2
	movslq -4(%1),%5
	movl %5,24(%2)
	ldzwq $abs,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $aktionen,%7
	movslq 0(%7),%7
	subq %5,%7,%6
	ldzwq $aktionen,%5
	movl %6,0(%5)
	jmp .L133
.L132:
# if (ch == 'a') { ... } else ...
	# condition: ch == 'a'
	movslq -8(%1),%5
	subq $97,%5,%0
	jne .L135
.L134:
# aufi = aufi + i;
	ldzwq $aufi,%6
	movslq 0(%6),%6
	movslq -4(%1),%7
	addq %6,%7,%5
	ldzwq $aufi,%6
	movl %5,0(%6)
	# Range(&aufi, 28);
	# function call: Range(&aufi, 28)
	subq $40,%2,%2
	ldzwq $aufi,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# aktionen = aktionen - abs(i);
	# function call: abs(i)
	subq $40,%2,%2
	movslq -4(%1),%5
	movl %5,24(%2)
	ldzwq $abs,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $aktionen,%7
	movslq 0(%7),%7
	subq %5,%7,%6
	ldzwq $aktionen,%5
	movl %6,0(%5)
	jmp .L136
.L135:
# if (ch == 'l') { ... } else ...
	# condition: ch == 'l'
	movslq -8(%1),%5
	subq $108,%5,%0
	jne .L138
.L137:
# lebi = lebi + i;
	ldzwq $lebi,%6
	movslq 0(%6),%6
	movslq -4(%1),%7
	addq %6,%7,%5
	ldzwq $lebi,%6
	movl %5,0(%6)
	# Range(&lebi, 28);
	# function call: Range(&lebi, 28)
	subq $40,%2,%2
	ldzwq $lebi,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# aktionen = aktionen - abs(i);
	# function call: abs(i)
	subq $40,%2,%2
	movslq -4(%1),%5
	movl %5,24(%2)
	ldzwq $abs,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $aktionen,%7
	movslq 0(%7),%7
	subq %5,%7,%6
	ldzwq $aktionen,%5
	movl %6,0(%5)
	jmp .L139
.L138:
# if (ch == 'v') { ... } else ...
	# condition: ch == 'v'
	movslq -8(%1),%5
	subq $118,%5,%0
	jne .L141
.L140:
# veri = veri + i;
	ldzwq $veri,%6
	movslq 0(%6),%6
	movslq -4(%1),%7
	addq %6,%7,%5
	ldzwq $veri,%6
	movl %5,0(%6)
	# Range(&veri, 28);
	# function call: Range(&veri, 28)
	subq $40,%2,%2
	ldzwq $veri,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# aktionen = aktionen - abs(i);
	# function call: abs(i)
	subq $40,%2,%2
	movslq -4(%1),%5
	movl %5,24(%2)
	ldzwq $abs,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $aktionen,%7
	movslq 0(%7),%7
	subq %5,%7,%6
	ldzwq $aktionen,%5
	movl %6,0(%5)
	jmp .L142
.L141:
# if (ch == 'n') { ... } else ...
	# condition: ch == 'n'
	movslq -8(%1),%5
	subq $110,%5,%0
	jne .L144
.L143:
# News();
	# function call: News()
	subq $24,%2,%2
	ldzwq $News,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	jmp .L145
.L144:
# if (ch == 'h') { ... } else ...
	# condition: ch == 'h'
	movslq -8(%1),%5
	subq $104,%5,%0
	jne .L147
.L146:
# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-8(%1)
	# while (ch == ' ') { ...
	jmp .L149
.L150:
# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-8(%1)
	# condition: ch == ' '
.L149:
	movslq -8(%1),%5
	subq $32,%5,%0
	je .L150
.L151:
# ... } // while (ch == ' ')
	# if (i == 0) { ...
	# condition: i == 0
	movslq -4(%1),%5
	subq $0,%5,%0
	jne .L152
.L153:
# i = hist_length - 1;
	ldzwq $hist_length,%6
	movslq 0(%6),%6
	subq $1,%6,%5
	movl %5,-4(%1)
.L152:
# ... } // if (i == 0)
	# if (ch == 'w') { ... } else ...
	# condition: ch == 'w'
	movslq -8(%1),%5
	subq $119,%5,%0
	jne .L155
.L154:
# PrintHistory(pol, i);
	# function call: PrintHistory(pol, i)
	subq $32,%2,%2
	ldzwq $pol,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	movslq -4(%1),%5
	movl %5,20(%2)
	ldzwq $PrintHistory,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L156
.L155:
# if (ch == 'l') { ... } else ...
	# condition: ch == 'l'
	movslq -8(%1),%5
	subq $108,%5,%0
	jne .L158
.L157:
# PrintHistory(leb, i);
	# function call: PrintHistory(leb, i)
	subq $32,%2,%2
	ldzwq $leb,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	movslq -4(%1),%5
	movl %5,20(%2)
	ldzwq $PrintHistory,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L159
.L158:
# if (ch == 'u') { ... } else ...
	# condition: ch == 'u'
	movslq -8(%1),%5
	subq $117,%5,%0
	jne .L161
.L160:
# PrintHistory(umw, i);
	# function call: PrintHistory(umw, i)
	subq $32,%2,%2
	ldzwq $umw,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	movslq -4(%1),%5
	movl %5,20(%2)
	ldzwq $PrintHistory,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L162
.L161:
# if (ch == 'p') { ... } else ...
	# condition: ch == 'p'
	movslq -8(%1),%5
	subq $112,%5,%0
	jne .L164
.L163:
# PrintHistory(prod, i);
	# function call: PrintHistory(prod, i)
	subq $32,%2,%2
	ldzwq $prod,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	movslq -4(%1),%5
	movl %5,20(%2)
	ldzwq $PrintHistory,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L165
.L164:
# if (ch == 'b') { ... } else ...
	# condition: ch == 'b'
	movslq -8(%1),%5
	subq $98,%5,%0
	jne .L167
.L166:
# PrintHistory(bev, i);
	# function call: PrintHistory(bev, i)
	subq $32,%2,%2
	ldzwq $bev,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	movslq -4(%1),%5
	movl %5,20(%2)
	ldzwq $PrintHistory,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L168
.L167:
# puts("w\tWahlergebnisse\n");
	# function call: puts("w\tWahlergebnisse\n")
	subq $32,%2,%2
	ldzwq $.L169,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("l\tLebensqualität\n");
	# function call: puts("l\tLebensqualität\n")
	subq $32,%2,%2
	ldzwq $.L170,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("u\tUmweltzustand\n");
	# function call: puts("u\tUmweltzustand\n")
	subq $32,%2,%2
	ldzwq $.L171,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("p\tProduktion\n");
	# function call: puts("p\tProduktion\n")
	subq $32,%2,%2
	ldzwq $.L172,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("b\tBevölkerung\n");
	# function call: puts("b\tBevölkerung\n")
	subq $32,%2,%2
	ldzwq $.L173,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
.L168:
# ... } // if (ch == 'b')
	
.L165:
# ... } // if (ch == 'p')
	
.L162:
# ... } // if (ch == 'u')
	
.L159:
# ... } // if (ch == 'l')
	
.L156:
# ... } // if (ch == 'w')
	jmp .L148
.L147:
# if (ch == '?') { ... } else ...
	# condition: ch == '?'
	movslq -8(%1),%5
	subq $63,%5,%0
	jne .L175
.L174:
# puts("Sanierung         s\n");
	# function call: puts("Sanierung         s\n")
	subq $32,%2,%2
	ldzwq $.L177,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Produktion        p\n");
	# function call: puts("Produktion        p\n")
	subq $32,%2,%2
	ldzwq $.L178,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Aufklärung        a\n");
	# function call: puts("Aufklärung        a\n")
	subq $32,%2,%2
	ldzwq $.L179,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Lebensqualität    l\n");
	# function call: puts("Lebensqualität    l\n")
	subq $32,%2,%2
	ldzwq $.L180,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Vermehrungsrate   v\n");
	# function call: puts("Vermehrungsrate   v\n")
	subq $32,%2,%2
	ldzwq $.L181,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Aktionsende       x\n");
	# function call: puts("Aktionsende       x\n")
	subq $32,%2,%2
	ldzwq $.L182,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("News              n\n");
	# function call: puts("News              n\n")
	subq $32,%2,%2
	ldzwq $.L183,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L176
.L175:
# if (ch == 'x') { ... } else ...
	# condition: ch == 'x'
	movslq -8(%1),%5
	subq $120,%5,%0
	jne .L185
.L184:
# break
	jmp .L102
	jmp .L186
.L185:
# puts("Unbekannte Aktionsmöglichkeit\n");
	# function call: puts("Unbekannte Aktionsmöglichkeit\n")
	subq $32,%2,%2
	ldzwq $.L187,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
.L186:
# ... } // if (ch == 'x')
	
.L176:
# ... } // if (ch == '?')
	
.L148:
# ... } // if (ch == 'h')
	
.L145:
# ... } // if (ch == 'n')
	
.L142:
# ... } // if (ch == 'v')
	
.L139:
# ... } // if (ch == 'l')
	
.L136:
# ... } // if (ch == 'a')
	
.L133:
# ... } // if (ch == 'p')
	
.L130:
# ... } // if (ch == 's')
	
.L127:
# ... } // if (ch == '\n')
	
.L122:
# ... } // if (abs(i) > aktionen && ch != 'h')
	# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-8(%1)
	# while (ch != '\n') { ...
	jmp .L188
.L189:
# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-8(%1)
	# condition: ch != '\n'
.L188:
	movslq -8(%1),%5
	subq $10,%5,%0
	jne .L189
.L190:
# ... } // while (ch != '\n')
	jmp .L107
.L106:
# break
	jmp .L102
.L107:
# ... } // if (aktionen > 0)
	# condition: true
.L100:
	ldzwq $1,%5
	subq %0,%5,%0
	jne .L101
.L102:
# ... } // while (true)
	
# return from function Tuwat:
.L78:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Events
# function header of void Events():
Events:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
	subq $8,%2,%2
# function body of Events:
	# if (prodi > 10) { ...
	# condition: prodi > 10
	ldzwq $prodi,%5
	movslq 0(%5),%5
	subq $10,%5,%0
	jle .L192
.L193:
# gift = gift + Random(0, 3) / 3;
	# function call: Random(0, 3)
	subq $40,%2,%2
	ldzwq $0,%5
	movl %5,24(%2)
	ldzwq $3,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	idivq $3,%5,%6
	ldzwq $gift,%7
	movslq 0(%7),%7
	addq %7,%6,%5
	ldzwq $gift,%6
	movl %5,0(%6)
.L192:
# ... } // if (prodi > 10)
	# if (krieg) { ...
	# condition: krieg
	ldzwq $krieg,%5
	movzbq 0(%5),%5
	subq %0,%5,%0
	je .L194
.L195:
# veri = veri / 2;
	ldzwq $veri,%7
	movslq 0(%7),%7
	idivq $2,%7,%5
	ldzwq $veri,%6
	movl %5,0(%6)
	# prodi = prodi - Random(0, 5);
	# function call: Random(0, 5)
	subq $40,%2,%2
	ldzwq $0,%5
	movl %5,24(%2)
	ldzwq $5,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $prodi,%7
	movslq 0(%7),%7
	subq %5,%7,%6
	ldzwq $prodi,%5
	movl %6,0(%5)
	# Range(&prodi, 28);
	# function call: Range(&prodi, 28)
	subq $40,%2,%2
	ldzwq $prodi,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# if (Random(32, 732) <= prodi * prodi / 2) { ... } else ...
	# condition: Random(32, 732) <= prodi * prodi / 2
	# function call: Random(32, 732)
	subq $40,%2,%2
	ldzwq $32,%5
	movl %5,24(%2)
	ldzwq $732,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $prodi,%7
	movslq 0(%7),%7
	ldzwq $prodi,%8
	movslq 0(%8),%8
	imulq %7,%8,%6
	idivq $2,%6,%7
	subq %7,%5,%0
	jg .L197
.L196:
# puts("Aufgrund des heftigen Widerstandes bricht die gegnerische\n");
	# function call: puts("Aufgrund des heftigen Widerstandes bricht die gegnerische\n")
	subq $32,%2,%2
	ldzwq $.L199,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Offensive zusammen. Der UN-Generalsekretär vermittelt den\n");
	# function call: puts("Offensive zusammen. Der UN-Generalsekretär vermittelt den\n")
	subq $32,%2,%2
	ldzwq $.L200,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Abschluss eines Friedensvertrages.\n");
	# function call: puts("Abschluss eines Friedensvertrages.\n")
	subq $32,%2,%2
	ldzwq $.L201,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# krieg = false;
	ldzwq $0,%5
	ldzwq $krieg,%6
	movb %5,0(%6)
	jmp .L198
.L197:
# puts("Die Kämpfe fordern schwere Verluste.\n");
	# function call: puts("Die Kämpfe fordern schwere Verluste.\n")
	subq $32,%2,%2
	ldzwq $.L202,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
.L198:
# ... } // if (Random(32, 732) <= prodi * prodi / 2)
	
.L194:
# ... } // if (krieg)
	# if (runden < 5) { ...
	# condition: runden < 5
	ldzwq $runden,%5
	movslq 0(%5),%5
	subq $5,%5,%0
	jge .L203
.L204:
	jmp .L191
.L203:
# ... } // if (runden < 5)
	# if (Random(lebi, 30) < 10) { ...
	# condition: Random(lebi, 30) < 10
	# function call: Random(lebi, 30)
	subq $40,%2,%2
	ldzwq $lebi,%5
	movslq 0(%5),%5
	movl %5,24(%2)
	ldzwq $30,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	subq $10,%5,%0
	jge .L205
.L206:
# puts("Unzufriedene Massen demonstrieren in der Hauptstadt.\n");
	# function call: puts("Unzufriedene Massen demonstrieren in der Hauptstadt.\n")
	subq $32,%2,%2
	ldzwq $.L207,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
.L205:
# ... } // if (Random(lebi, 30) < 10)
	# if (lebi < 5 && Random(0, lebi) == 0) { ...
	# condition: lebi < 5 && Random(0, lebi) == 0
	ldzwq $lebi,%5
	movslq 0(%5),%5
	subq $5,%5,%0
	jge .L208
	# function call: Random(0, lebi)
	subq $40,%2,%2
	ldzwq $0,%5
	movl %5,24(%2)
	ldzwq $lebi,%5
	movslq 0(%5),%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	subq $0,%5,%0
	jne .L208
.L209:
# puts("Sie entgehen nur knapp einem Anschlag auf Ihr Leben.\n");
	# function call: puts("Sie entgehen nur knapp einem Anschlag auf Ihr Leben.\n")
	subq $32,%2,%2
	ldzwq $.L211,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
.L208:
# ... } // if (lebi < 5 && Random(0, lebi) == 0)
	# function call: Random(1, 30)
	subq $40,%2,%2
	ldzwq $1,%5
	movl %5,24(%2)
	ldzwq $30,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	# initializer assigns Random(1, 30) to select + 0
	movl %5,-4(%1)
	# if (select <= 2) { ... } else ...
	# condition: select <= 2
	movslq -4(%1),%5
	subq $2,%5,%0
	jg .L213
.L212:
# if (poli >= 20) { ...
	# condition: poli >= 20
	ldzwq $poli,%5
	movslq 0(%5),%5
	subq $20,%5,%0
	jl .L215
.L216:
# if (Random(0, 1) == 0) { ... } else ...
	# condition: Random(0, 1) == 0
	# function call: Random(0, 1)
	subq $40,%2,%2
	ldzwq $0,%5
	movl %5,24(%2)
	ldzwq $1,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	subq $0,%5,%0
	jne .L218
.L217:
# puts("Einer Ihrer Minister ist in einen Skandal verwickelt\n");
	# function call: puts("Einer Ihrer Minister ist in einen Skandal verwickelt\n")
	subq $32,%2,%2
	ldzwq $.L220,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("und tritt zurück.\n");
	# function call: puts("und tritt zurück.\n")
	subq $32,%2,%2
	ldzwq $.L221,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L219
.L218:
# puts("Der Landwirtschaftsminister wird der Korruption überführt\n");
	# function call: puts("Der Landwirtschaftsminister wird der Korruption überführt\n")
	subq $32,%2,%2
	ldzwq $.L222,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("und gesteht die Fälschung von Statistiken und ");
	# function call: puts("und gesteht die Fälschung von Statistiken und ")
	subq $32,%2,%2
	ldzwq $.L223,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Hinterziehung\n der Sanierungsgelder.\n");
	# function call: puts("Hinterziehung\n der Sanierungsgelder.\n")
	subq $32,%2,%2
	ldzwq $.L224,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# sani = 0;
	ldzwq $0,%5
	ldzwq $sani,%6
	movl %5,0(%6)
	# umwi = umwi + 10;
	ldzwq $umwi,%6
	movslq 0(%6),%6
	addq $10,%6,%5
	ldzwq $umwi,%6
	movl %5,0(%6)
	# Range(&umwi, 28);
	# function call: Range(&umwi, 28)
	subq $40,%2,%2
	ldzwq $umwi,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
.L219:
# ... } // if (Random(0, 1) == 0)
	# poli = poli - 12;
	ldzwq $poli,%6
	movslq 0(%6),%6
	subq $12,%6,%5
	ldzwq $poli,%6
	movl %5,0(%6)
.L215:
# ... } // if (poli >= 20)
	jmp .L214
.L213:
# if (select <= 5) { ... } else ...
	# condition: select <= 5
	movslq -4(%1),%5
	subq $5,%5,%0
	jg .L226
.L225:
# if (gift > 0) { ...
	# condition: gift > 0
	ldzwq $gift,%5
	movslq 0(%5),%5
	subq $0,%5,%0
	jle .L228
.L229:
# puts("Auf einer Mülldeponie werden hochgiftige Substanzen frei,\n");
	# function call: puts("Auf einer Mülldeponie werden hochgiftige Substanzen frei,\n")
	subq $32,%2,%2
	ldzwq $.L230,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("die dort illegal lagerten.\n");
	# function call: puts("die dort illegal lagerten.\n")
	subq $32,%2,%2
	ldzwq $.L231,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# poli = poli - 3;
	ldzwq $poli,%6
	movslq 0(%6),%6
	subq $3,%6,%5
	ldzwq $poli,%6
	movl %5,0(%6)
	# Range(&poli, 47);
	# function call: Range(&poli, 47)
	subq $40,%2,%2
	ldzwq $poli,%5
	movq %5,16(%2)
	ldzwq $47,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# umwi = umwi + 3;
	ldzwq $umwi,%6
	movslq 0(%6),%6
	addq $3,%6,%5
	ldzwq $umwi,%6
	movl %5,0(%6)
	# Range(&umwi, 28);
	# function call: Range(&umwi, 28)
	subq $40,%2,%2
	ldzwq $umwi,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# gift = gift - 1;
	ldzwq $gift,%6
	movslq 0(%6),%6
	subq $1,%6,%5
	ldzwq $gift,%6
	movl %5,0(%6)
.L228:
# ... } // if (gift > 0)
	jmp .L227
.L226:
# if (select <= 7) { ... } else ...
	# condition: select <= 7
	movslq -4(%1),%5
	subq $7,%5,%0
	jg .L233
.L232:
# if (gift > 0) { ...
	# condition: gift > 0
	ldzwq $gift,%5
	movslq 0(%5),%5
	subq $0,%5,%0
	jle .L235
.L236:
# puts("Bei einem Unfall in einem Kernkraftwerk werden hochaktive ");
	# function call: puts("Bei einem Unfall in einem Kernkraftwerk werden hochaktive ")
	subq $32,%2,%2
	ldzwq $.L237,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Substanzen frei.\n");
	# function call: puts("Substanzen frei.\n")
	subq $32,%2,%2
	ldzwq $.L238,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# bevi = bevi - 1;
	ldzwq $bevi,%6
	movslq 0(%6),%6
	subq $1,%6,%5
	ldzwq $bevi,%6
	movl %5,0(%6)
	# Range(&bevi, 28);
	# function call: Range(&bevi, 28)
	subq $40,%2,%2
	ldzwq $bevi,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# umwi = umwi + 5;
	ldzwq $umwi,%6
	movslq 0(%6),%6
	addq $5,%6,%5
	ldzwq $umwi,%6
	movl %5,0(%6)
	# Range(&umwi, 28);
	# function call: Range(&umwi, 28)
	subq $40,%2,%2
	ldzwq $umwi,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# poli = 3 * poli / 4;
	ldzwq $3,%6
	ldzwq $poli,%7
	movslq 0(%7),%7
	imulq %6,%7,%5
	idivq $4,%5,%6
	ldzwq $poli,%5
	movl %6,0(%5)
	# gift = gift - 1;
	ldzwq $gift,%6
	movslq 0(%6),%6
	subq $1,%6,%5
	ldzwq $gift,%6
	movl %5,0(%6)
.L235:
# ... } // if (gift > 0)
	jmp .L234
.L233:
# if (select <= 9) { ... } else ...
	# condition: select <= 9
	movslq -4(%1),%5
	subq $9,%5,%0
	jg .L240
.L239:
# if (Random(0, aufi) > 10) { ...
	# condition: Random(0, aufi) > 10
	# function call: Random(0, aufi)
	subq $40,%2,%2
	ldzwq $0,%5
	movl %5,24(%2)
	ldzwq $aufi,%5
	movslq 0(%5),%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	subq $10,%5,%0
	jle .L242
.L243:
# *(*(produktion + rot) + prodi) = *(*(produktion + rot) + prodi) + 1;
	ldzwq $116,%5
	ldzwq $rot,%6
	movslq 0(%6),%6
	imulq %6,%5,%6
	ldzwq $produktion,%7
	addq %7,%6,%5
	ldzwq $4,%6
	ldzwq $prodi,%7
	movslq 0(%7),%7
	imulq %7,%6,%7
	addq %5,%7,%6
	ldzwq $116,%5
	ldzwq $rot,%7
	movslq 0(%7),%7
	imulq %7,%5,%7
	ldzwq $produktion,%8
	addq %8,%7,%5
	ldzwq $4,%7
	ldzwq $prodi,%8
	movslq 0(%8),%8
	imulq %8,%7,%8
	addq %5,%8,%7
	movslq 0(%7),%7
	addq $1,%7,%5
	movl %5,0(%6)
.L242:
# ... } // if (Random(0, aufi) > 10)
	jmp .L241
.L240:
# if (select <= 10) { ... } else ...
	# condition: select <= 10
	movslq -4(%1),%5
	subq $10,%5,%0
	jg .L245
.L244:
# puts("Eine bislang unbekannte Krankheit breitet sich epidemisch aus.\n");
	# function call: puts("Eine bislang unbekannte Krankheit breitet sich epidemisch aus.\n")
	subq $32,%2,%2
	ldzwq $.L247,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# bevi = (bevi * 3 + 1) / 4;
	ldzwq $bevi,%6
	movslq 0(%6),%6
	imulq $3,%6,%5
	addq $1,%5,%6
	idivq $4,%6,%7
	ldzwq $bevi,%5
	movl %7,0(%5)
	# veri = aufi / 2;
	ldzwq $aufi,%7
	movslq 0(%7),%7
	idivq $2,%7,%5
	ldzwq $veri,%6
	movl %5,0(%6)
	# lebi = lebi - 10;
	ldzwq $lebi,%6
	movslq 0(%6),%6
	subq $10,%6,%5
	ldzwq $lebi,%6
	movl %5,0(%6)
	# Range(&lebi, 28);
	# function call: Range(&lebi, 28)
	subq $40,%2,%2
	ldzwq $lebi,%5
	movq %5,16(%2)
	ldzwq $28,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	jmp .L246
.L245:
# if (select <= 12) { ... } else ...
	# condition: select <= 12
	movslq -4(%1),%5
	subq $12,%5,%0
	jg .L249
.L248:
# if (!krieg) { ...
	# condition: !krieg
	ldzwq $krieg,%5
	movzbq 0(%5),%5
	subq %5,%0,%0
	jne .L251
.L252:
# puts("Einer Ihrer Nachbarstaaten erklärt Ihnen den Krieg.\n");
	# function call: puts("Einer Ihrer Nachbarstaaten erklärt Ihnen den Krieg.\n")
	subq $32,%2,%2
	ldzwq $.L253,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# krieg = true;
	ldzwq $1,%5
	ldzwq $krieg,%6
	movb %5,0(%6)
.L251:
# ... } // if (!krieg)
	jmp .L250
.L249:
# if (select <= 13) { ...
	# condition: select <= 13
	movslq -4(%1),%5
	subq $13,%5,%0
	jg .L254
.L255:
# if (krieg && prodi > 4) { ...
	# condition: krieg && prodi > 4
	ldzwq $krieg,%5
	movzbq 0(%5),%5
	subq %0,%5,%0
	je .L256
	ldzwq $prodi,%5
	movslq 0(%5),%5
	subq $4,%5,%0
	jle .L256
.L257:
# puts("Es lang dem Feind, die Front zu durchbrechen und ein ");
	# function call: puts("Es lang dem Feind, die Front zu durchbrechen und ein ")
	subq $32,%2,%2
	ldzwq $.L259,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Industriezentrum einzunehmen.\n");
	# function call: puts("Industriezentrum einzunehmen.\n")
	subq $32,%2,%2
	ldzwq $.L260,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# bevi = bevi - 1;
	ldzwq $bevi,%6
	movslq 0(%6),%6
	subq $1,%6,%5
	ldzwq $bevi,%6
	movl %5,0(%6)
	# Range(&bevi, 47);
	# function call: Range(&bevi, 47)
	subq $40,%2,%2
	ldzwq $bevi,%5
	movq %5,16(%2)
	ldzwq $47,%5
	movl %5,24(%2)
	ldzwq $Range,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	addq $40,%2,%2
	# prodi = prodi - 2;
	ldzwq $prodi,%6
	movslq 0(%6),%6
	subq $2,%6,%5
	ldzwq $prodi,%6
	movl %5,0(%6)
.L256:
# ... } // if (krieg && prodi > 4)
	
.L254:
# ... } // if (select <= 13)
	
.L250:
# ... } // if (select <= 12)
	
.L246:
# ... } // if (select <= 10)
	
.L241:
# ... } // if (select <= 9)
	
.L234:
# ... } // if (select <= 7)
	
.L227:
# ... } // if (select <= 5)
	
.L214:
# ... } // if (select <= 2)
	
# return from function Events:
.L191:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Start
# function header of void Start():
Start:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
	subq $8,%2,%2
# function body of Start:
	# puts("     *** O E K O P O L Y ***\n\n");
	# function call: puts("     *** O E K O P O L Y ***\n\n")
	subq $32,%2,%2
	ldzwq $.L262,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Sie können eines der folgenden Länder regieren:\n");
	# function call: puts("Sie können eines der folgenden Länder regieren:\n")
	subq $32,%2,%2
	ldzwq $.L263,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("\tTechnetia    t\n");
	# function call: puts("\tTechnetia    t\n")
	subq $32,%2,%2
	ldzwq $.L264,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("\tKybernetien  k\n");
	# function call: puts("\tKybernetien  k\n")
	subq $32,%2,%2
	ldzwq $.L265,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("\tAgrien       a\n");
	# function call: puts("\tAgrien       a\n")
	subq $32,%2,%2
	ldzwq $.L266,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("\tArminien     m\n");
	# function call: puts("\tArminien     m\n")
	subq $32,%2,%2
	ldzwq $.L267,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("\tArmageddon   g\n");
	# function call: puts("\tArmageddon   g\n")
	subq $32,%2,%2
	ldzwq $.L268,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("\tRandomia     r\n");
	# function call: puts("\tRandomia     r\n")
	subq $32,%2,%2
	ldzwq $.L269,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# krieg = false;
	ldzwq $0,%5
	ldzwq $krieg,%6
	movb %5,0(%6)
	# gift = 0;
	ldzwq $0,%5
	ldzwq $gift,%6
	movl %5,0(%6)
	# while (true) { ...
	jmp .L270
.L271:
# puts("Kennbuchstabe: ");
	# function call: puts("Kennbuchstabe: ")
	subq $32,%2,%2
	ldzwq $.L273,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	# initializer assigns getchar() to ch + 0
	movl %5,-4(%1)
	# while (ch == ' ') { ...
	jmp .L274
.L275:
# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-4(%1)
	# condition: ch == ' '
.L274:
	movslq -4(%1),%5
	subq $32,%5,%0
	je .L275
.L276:
# ... } // while (ch == ' ')
	# if (ch == 'k') { ... } else ...
	# condition: ch == 'k'
	movslq -4(%1),%5
	subq $107,%5,%0
	jne .L278
.L277:
# sani = 0;
	ldzwq $0,%5
	ldzwq $sani,%6
	movl %5,0(%6)
	# prodi = 11;
	ldzwq $11,%5
	ldzwq $prodi,%6
	movl %5,0(%6)
	# umwi = 12;
	ldzwq $12,%5
	ldzwq $umwi,%6
	movl %5,0(%6)
	# aufi = 7;
	ldzwq $7,%5
	ldzwq $aufi,%6
	movl %5,0(%6)
	# lebi = 9;
	ldzwq $9,%5
	ldzwq $lebi,%6
	movl %5,0(%6)
	# veri = 19;
	ldzwq $19,%5
	ldzwq $veri,%6
	movl %5,0(%6)
	# bevi = 20;
	ldzwq $20,%5
	ldzwq $bevi,%6
	movl %5,0(%6)
	# poli = 10;
	ldzwq $10,%5
	ldzwq $poli,%6
	movl %5,0(%6)
	# break
	jmp .L272
	jmp .L279
.L278:
# if (ch == 't') { ... } else ...
	# condition: ch == 't'
	movslq -4(%1),%5
	subq $116,%5,%0
	jne .L281
.L280:
# sani = 2;
	ldzwq $2,%5
	ldzwq $sani,%6
	movl %5,0(%6)
	# prodi = 22;
	ldzwq $22,%5
	ldzwq $prodi,%6
	movl %5,0(%6)
	# umwi = 15;
	ldzwq $15,%5
	ldzwq $umwi,%6
	movl %5,0(%6)
	# aufi = 3;
	ldzwq $3,%5
	ldzwq $aufi,%6
	movl %5,0(%6)
	# lebi = 5;
	ldzwq $5,%5
	ldzwq $lebi,%6
	movl %5,0(%6)
	# veri = 22;
	ldzwq $22,%5
	ldzwq $veri,%6
	movl %5,0(%6)
	# bevi = 32;
	ldzwq $32,%5
	ldzwq $bevi,%6
	movl %5,0(%6)
	# poli = 10;
	ldzwq $10,%5
	ldzwq $poli,%6
	movl %5,0(%6)
	# gift = 5;
	ldzwq $5,%5
	ldzwq $gift,%6
	movl %5,0(%6)
	# break
	jmp .L272
	jmp .L282
.L281:
# if (ch == 'a') { ... } else ...
	# condition: ch == 'a'
	movslq -4(%1),%5
	subq $97,%5,%0
	jne .L284
.L283:
# sani = 0;
	ldzwq $0,%5
	ldzwq $sani,%6
	movl %5,0(%6)
	# prodi = 4;
	ldzwq $4,%5
	ldzwq $prodi,%6
	movl %5,0(%6)
	# umwi = 6;
	ldzwq $6,%5
	ldzwq $umwi,%6
	movl %5,0(%6)
	# aufi = 0;
	ldzwq $0,%5
	ldzwq $aufi,%6
	movl %5,0(%6)
	# lebi = 12;
	ldzwq $12,%5
	ldzwq $lebi,%6
	movl %5,0(%6)
	# veri = 13;
	ldzwq $13,%5
	ldzwq $veri,%6
	movl %5,0(%6)
	# bevi = 12;
	ldzwq $12,%5
	ldzwq $bevi,%6
	movl %5,0(%6)
	# poli = 10;
	ldzwq $10,%5
	ldzwq $poli,%6
	movl %5,0(%6)
	# break
	jmp .L272
	jmp .L285
.L284:
# if (ch == 'm') { ... } else ...
	# condition: ch == 'm'
	movslq -4(%1),%5
	subq $109,%5,%0
	jne .L287
.L286:
# sani = 0;
	ldzwq $0,%5
	ldzwq $sani,%6
	movl %5,0(%6)
	# prodi = 0;
	ldzwq $0,%5
	ldzwq $prodi,%6
	movl %5,0(%6)
	# umwi = 6;
	ldzwq $6,%5
	ldzwq $umwi,%6
	movl %5,0(%6)
	# aufi = 0;
	ldzwq $0,%5
	ldzwq $aufi,%6
	movl %5,0(%6)
	# lebi = 5;
	ldzwq $5,%5
	ldzwq $lebi,%6
	movl %5,0(%6)
	# veri = 22;
	ldzwq $22,%5
	ldzwq $veri,%6
	movl %5,0(%6)
	# bevi = 2;
	ldzwq $2,%5
	ldzwq $bevi,%6
	movl %5,0(%6)
	# poli = 12;
	ldzwq $12,%5
	ldzwq $poli,%6
	movl %5,0(%6)
	# break
	jmp .L272
	jmp .L288
.L287:
# if (ch == 'g') { ... } else ...
	# condition: ch == 'g'
	movslq -4(%1),%5
	subq $103,%5,%0
	jne .L290
.L289:
# sani = 10;
	ldzwq $10,%5
	ldzwq $sani,%6
	movl %5,0(%6)
	# prodi = 24;
	ldzwq $24,%5
	ldzwq $prodi,%6
	movl %5,0(%6)
	# umwi = 15;
	ldzwq $15,%5
	ldzwq $umwi,%6
	movl %5,0(%6)
	# aufi = 9;
	ldzwq $9,%5
	ldzwq $aufi,%6
	movl %5,0(%6)
	# lebi = 14;
	ldzwq $14,%5
	ldzwq $lebi,%6
	movl %5,0(%6)
	# veri = 23;
	ldzwq $23,%5
	ldzwq $veri,%6
	movl %5,0(%6)
	# bevi = 29;
	ldzwq $29,%5
	ldzwq $bevi,%6
	movl %5,0(%6)
	# poli = 15;
	ldzwq $15,%5
	ldzwq $poli,%6
	movl %5,0(%6)
	# krieg = true;
	ldzwq $1,%5
	ldzwq $krieg,%6
	movb %5,0(%6)
	# gift = 1;
	ldzwq $1,%5
	ldzwq $gift,%6
	movl %5,0(%6)
	# puts("Armaggedon befindet sich im Kriegszustand.\n");
	# function call: puts("Armaggedon befindet sich im Kriegszustand.\n")
	subq $32,%2,%2
	ldzwq $.L292,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# break
	jmp .L272
	jmp .L291
.L290:
# if (ch == 'r') { ... } else ...
	# condition: ch == 'r'
	movslq -4(%1),%5
	subq $114,%5,%0
	jne .L294
.L293:
# sani = Random(3, 20);
	# function call: Random(3, 20)
	subq $40,%2,%2
	ldzwq $3,%5
	movl %5,24(%2)
	ldzwq $20,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $sani,%6
	movl %5,0(%6)
	# prodi = Random(3, 20);
	# function call: Random(3, 20)
	subq $40,%2,%2
	ldzwq $3,%5
	movl %5,24(%2)
	ldzwq $20,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $prodi,%6
	movl %5,0(%6)
	# umwi = Random(3, 20);
	# function call: Random(3, 20)
	subq $40,%2,%2
	ldzwq $3,%5
	movl %5,24(%2)
	ldzwq $20,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $umwi,%6
	movl %5,0(%6)
	# aufi = Random(0, 5);
	# function call: Random(0, 5)
	subq $40,%2,%2
	ldzwq $0,%5
	movl %5,24(%2)
	ldzwq $5,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $aufi,%6
	movl %5,0(%6)
	# lebi = Random(3, 20);
	# function call: Random(3, 20)
	subq $40,%2,%2
	ldzwq $3,%5
	movl %5,24(%2)
	ldzwq $20,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $lebi,%6
	movl %5,0(%6)
	# veri = Random(0, 28);
	# function call: Random(0, 28)
	subq $40,%2,%2
	ldzwq $0,%5
	movl %5,24(%2)
	ldzwq $28,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $veri,%6
	movl %5,0(%6)
	# bevi = Random(3, 20);
	# function call: Random(3, 20)
	subq $40,%2,%2
	ldzwq $3,%5
	movl %5,24(%2)
	ldzwq $20,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $bevi,%6
	movl %5,0(%6)
	# poli = Random(5, 15);
	# function call: Random(5, 15)
	subq $40,%2,%2
	ldzwq $5,%5
	movl %5,24(%2)
	ldzwq $15,%5
	movl %5,28(%2)
	ldzwq $Random,%5
	movq %4,32(%2)
	jmp %5,%3
	movq 32(%2),%4
	movslq 16(%2),%5
	addq $40,%2,%2
	ldzwq $poli,%6
	movl %5,0(%6)
	# break
	jmp .L272
	jmp .L295
.L294:
# if (ch == '\n') { ... } else ...
	# condition: ch == '\n'
	movslq -4(%1),%5
	subq $10,%5,%0
	jne .L297
.L296:
	jmp .L298
.L297:
# puts("Unbekannter Kennbuchstabe!\n");
	# function call: puts("Unbekannter Kennbuchstabe!\n")
	subq $32,%2,%2
	ldzwq $.L299,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# while (ch != '\n') { ...
	jmp .L300
.L301:
# ch = getchar();
	# function call: getchar()
	subq $32,%2,%2
	ldzwq $getchar,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movslq 16(%2),%5
	addq $32,%2,%2
	movl %5,-4(%1)
	# condition: ch != '\n'
.L300:
	movslq -4(%1),%5
	subq $10,%5,%0
	jne .L301
.L302:
# ... } // while (ch != '\n')
	
.L298:
# ... } // if (ch == '\n')
	
.L295:
# ... } // if (ch == 'r')
	
.L291:
# ... } // if (ch == 'g')
	
.L288:
# ... } // if (ch == 'm')
	
.L285:
# ... } // if (ch == 'a')
	
.L282:
# ... } // if (ch == 't')
	
.L279:
# ... } // if (ch == 'k')
	# condition: true
.L270:
	ldzwq $1,%5
	subq %0,%5,%0
	jne .L271
.L272:
# ... } // while (true)
	# aktionen = 5;
	ldzwq $5,%5
	ldzwq $aktionen,%6
	movl %5,0(%6)
	# runden = 0;
	ldzwq $0,%5
	ldzwq $runden,%6
	movl %5,0(%6)
# return from function Start:
.L261:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Bilanz
# function header of void Bilanz():
Bilanz:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
	subq $8,%2,%2
# function body of Bilanz:
	# puts("\n\n");
	# function call: puts("\n\n")
	subq $32,%2,%2
	ldzwq $.L304,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("... und das meint der Fachmann zu Ihrer Leistung:\n");
	# function call: puts("... und das meint der Fachmann zu Ihrer Leistung:\n")
	subq $32,%2,%2
	ldzwq $.L305,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	ldzwq $poli,%6
	movslq 0(%6),%6
	subq $10,%6,%5
	ldzwq $116,%6
	ldzwq $gruen,%7
	movslq 0(%7),%7
	imulq %7,%6,%7
	ldzwq $lebensqualitaet,%8
	addq %8,%7,%6
	ldzwq $4,%7
	ldzwq $lebi,%8
	movslq 0(%8),%8
	imulq %8,%7,%8
	addq %6,%8,%7
	ldzwq $3,%8
	movslq 0(%7),%7
	imulq %8,%7,%6
	addq %5,%6,%7
	ldzwq $10,%6
	imulq %6,%7,%5
	# initializer assigns 10 * (poli - 10 + 3 * *(*(lebensqualitaet + gruen) + lebi)) to saldo + 0
	movl %5,-4(%1)
	# saldo = saldo + 5 * bevi;
	ldzwq $5,%6
	ldzwq $bevi,%7
	movslq 0(%7),%7
	imulq %6,%7,%5
	movslq -4(%1),%7
	addq %7,%5,%6
	movl %6,-4(%1)
	# saldo = saldo - (runden + 3);
	ldzwq $runden,%6
	movslq 0(%6),%6
	addq $3,%6,%5
	movslq -4(%1),%7
	subq %5,%7,%6
	movl %6,-4(%1)
	# if (saldo < 0) { ... } else ...
	# condition: saldo < 0
	movslq -4(%1),%5
	subq $0,%5,%0
	jge .L307
.L306:
# puts("Sie werden in die Geschichte eingehen -- ");
	# function call: puts("Sie werden in die Geschichte eingehen -- ")
	subq $32,%2,%2
	ldzwq $.L309,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("als 5. Apokalyptischer Reiter...\n");
	# function call: puts("als 5. Apokalyptischer Reiter...\n")
	subq $32,%2,%2
	ldzwq $.L310,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L308
.L307:
# if (saldo <= 30) { ... } else ...
	# condition: saldo <= 30
	movslq -4(%1),%5
	subq $30,%5,%0
	jg .L312
.L311:
# puts("Für wen arbeiten Sie eigentlich?\n");
	# function call: puts("Für wen arbeiten Sie eigentlich?\n")
	subq $32,%2,%2
	ldzwq $.L314,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L313
.L312:
# if (saldo <= 50) { ... } else ...
	# condition: saldo <= 50
	movslq -4(%1),%5
	subq $50,%5,%0
	jg .L316
.L315:
# puts("Haben Sie einen Würfel oder eine Münze benutzt?\n");
	# function call: puts("Haben Sie einen Würfel oder eine Münze benutzt?\n")
	subq $32,%2,%2
	ldzwq $.L318,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L317
.L316:
# if (saldo <= 75) { ... } else ...
	# condition: saldo <= 75
	movslq -4(%1),%5
	subq $75,%5,%0
	jg .L320
.L319:
# puts("Sie zeigen vielversprechende Ansätze. Mehr üben!\n");
	# function call: puts("Sie zeigen vielversprechende Ansätze. Mehr üben!\n")
	subq $32,%2,%2
	ldzwq $.L322,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L321
.L320:
# if (saldo <= 100) { ... } else ...
	# condition: saldo <= 100
	movslq -4(%1),%5
	subq $100,%5,%0
	jg .L324
.L323:
# puts("Schade, dass Sie kein Politiker sind.\n");
	# function call: puts("Schade, dass Sie kein Politiker sind.\n")
	subq $32,%2,%2
	ldzwq $.L326,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L325
.L324:
# puts("Sind Sie ein Genie, oder haben Sie voher geübt?!\n");
	# function call: puts("Sind Sie ein Genie, oder haben Sie voher geübt?!\n")
	subq $32,%2,%2
	ldzwq $.L327,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
.L325:
# ... } // if (saldo <= 100)
	
.L321:
# ... } // if (saldo <= 75)
	
.L317:
# ... } // if (saldo <= 50)
	
.L313:
# ... } // if (saldo <= 30)
	
.L308:
# ... } // if (saldo < 0)
	
# return from function Bilanz:
.L303:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl Game
# function header of uint8_t Game():
Game:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of Game:
	# Enter(pol, poli);
	# function call: Enter(pol, poli)
	subq $32,%2,%2
	ldzwq $pol,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	ldzwq $poli,%5
	movslq 0(%5),%5
	movl %5,20(%2)
	ldzwq $Enter,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# Enter(leb, lebi);
	# function call: Enter(leb, lebi)
	subq $32,%2,%2
	ldzwq $leb,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	ldzwq $lebi,%5
	movslq 0(%5),%5
	movl %5,20(%2)
	ldzwq $Enter,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# Enter(umw, umwi);
	# function call: Enter(umw, umwi)
	subq $32,%2,%2
	ldzwq $umw,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	ldzwq $umwi,%5
	movslq 0(%5),%5
	movl %5,20(%2)
	ldzwq $Enter,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# Enter(prod, prodi);
	# function call: Enter(prod, prodi)
	subq $32,%2,%2
	ldzwq $prod,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	ldzwq $prodi,%5
	movslq 0(%5),%5
	movl %5,20(%2)
	ldzwq $Enter,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# Enter(bev, bevi);
	# function call: Enter(bev, bevi)
	subq $32,%2,%2
	ldzwq $bev,%5
	movslq 0(%5),%5
	movl %5,16(%2)
	ldzwq $bevi,%5
	movslq 0(%5),%5
	movl %5,20(%2)
	ldzwq $Enter,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# News();
	# function call: News()
	subq $24,%2,%2
	ldzwq $News,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# Tuwat();
	# function call: Tuwat()
	subq $24,%2,%2
	ldzwq $Tuwat,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# NewPeriod();
	# function call: NewPeriod()
	subq $24,%2,%2
	ldzwq $NewPeriod,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# runden = runden + 1;
	ldzwq $runden,%6
	movslq 0(%6),%6
	addq $1,%6,%5
	ldzwq $runden,%6
	movl %5,0(%6)
	# Zyklus();
	# function call: Zyklus()
	subq $24,%2,%2
	ldzwq $Zyklus,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# Events();
	# function call: Events()
	subq $24,%2,%2
	ldzwq $Events,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# if (poli < 0) { ... } else ...
	# condition: poli < 0
	ldzwq $poli,%5
	movslq 0(%5),%5
	subq $0,%5,%0
	jge .L330
.L329:
# if (lebi > 0) { ... } else ...
	# condition: lebi > 0
	ldzwq $lebi,%5
	movslq 0(%5),%5
	subq $0,%5,%0
	jle .L333
.L332:
# News();
	# function call: News()
	subq $24,%2,%2
	ldzwq $News,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# puts("Sie werden nicht mehr wiedergewählt.\n");
	# function call: puts("Sie werden nicht mehr wiedergewählt.\n")
	subq $32,%2,%2
	ldzwq $.L335,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	jmp .L334
.L333:
# puts("Es gelingt Ihnen nicht mehr, vor der Revolution ins Ausland ");
	# function call: puts("Es gelingt Ihnen nicht mehr, vor der Revolution ins Ausland ")
	subq $32,%2,%2
	ldzwq $.L336,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("zu flüchten.\n");
	# function call: puts("zu flüchten.\n")
	subq $32,%2,%2
	ldzwq $.L337,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Ihre Leiche wird in die Kanalisation geworfen.\n");
	# function call: puts("Ihre Leiche wird in die Kanalisation geworfen.\n")
	subq $32,%2,%2
	ldzwq $.L338,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
.L334:
# ... } // if (lebi > 0)
	ldzwq $0,%5
	movb %5,16(%1)
	jmp .L328
	jmp .L331
.L330:
# if (poli > 47) { ... } else ...
	# condition: poli > 47
	ldzwq $poli,%5
	movslq 0(%5),%5
	subq $47,%5,%0
	jle .L340
.L339:
# News();
	# function call: News()
	subq $24,%2,%2
	ldzwq $News,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# puts("Aufgrund Ihrer Erfolge werden Sie zum Präsidenten auf ");
	# function call: puts("Aufgrund Ihrer Erfolge werden Sie zum Präsidenten auf ")
	subq $32,%2,%2
	ldzwq $.L342,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Lebenszeit ernannt.\n");
	# function call: puts("Lebenszeit ernannt.\n")
	subq $32,%2,%2
	ldzwq $.L343,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	ldzwq $0,%5
	movb %5,16(%1)
	jmp .L328
	jmp .L341
.L340:
# if (bevi == 0) { ... } else ...
	# condition: bevi == 0
	ldzwq $bevi,%5
	movslq 0(%5),%5
	subq $0,%5,%0
	jne .L345
.L344:
# News();
	# function call: News()
	subq $24,%2,%2
	ldzwq $News,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# puts("Trotz Ihrer verzweifelten Anstrengungen leben nur noch ");
	# function call: puts("Trotz Ihrer verzweifelten Anstrengungen leben nur noch ")
	subq $32,%2,%2
	ldzwq $.L347,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("zwei Einwohner,\ndie Sie mit vereinten Kräften ");
	# function call: puts("zwei Einwohner,\ndie Sie mit vereinten Kräften ")
	subq $32,%2,%2
	ldzwq $.L348,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("über die Grenze befördern.\n");
	# function call: puts("über die Grenze befördern.\n")
	subq $32,%2,%2
	ldzwq $.L349,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	ldzwq $0,%5
	movb %5,16(%1)
	jmp .L328
	jmp .L346
.L345:
# if (runden >= 36) { ... } else ...
	# condition: runden >= 36
	ldzwq $runden,%5
	movslq 0(%5),%5
	subq $36,%5,%0
	jl .L351
.L350:
# puts("Aufgrund Ihres hohen Alters lehnen Sie eine weitere ");
	# function call: puts("Aufgrund Ihres hohen Alters lehnen Sie eine weitere ")
	subq $32,%2,%2
	ldzwq $.L353,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# puts("Kandidatur ab.\n");
	# function call: puts("Kandidatur ab.\n")
	subq $32,%2,%2
	ldzwq $.L354,%5
	movq %5,16(%2)
	ldzwq $puts,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	ldzwq $0,%5
	movb %5,16(%1)
	jmp .L328
	jmp .L352
.L351:
	ldzwq $1,%5
	movb %5,16(%1)
	jmp .L328
.L352:
# ... } // if (runden >= 36)
	
.L346:
# ... } // if (bevi == 0)
	
.L341:
# ... } // if (poli > 47)
	
.L331:
# ... } // if (poli < 0)
	
# return from function Game:
.L328:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.text
	.globl main
# function header of int32_t main():
main:
	movq %3,0(%2)
	movq %1,8(%2)
	addq $0,%2,%1
# function body of main:
	# srand(time());
	# function call: srand(time())
	subq $32,%2,%2
	# function call: time()
	subq $24,%2,%2
	ldzwq $time,%5
	movq %4,48(%2)
	jmp %5,%3
	movq 48(%2),%4
	movq 16(%2),%5
	addq $24,%2,%2
	movl %5,16(%2)
	ldzwq $srand,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	addq $32,%2,%2
	# Start();
	# function call: Start()
	subq $24,%2,%2
	ldzwq $Start,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	# while (Game()) { ...
	jmp .L356
.L357:
# condition: Game()
	
.L356:
# function call: Game()
	subq $32,%2,%2
	ldzwq $Game,%5
	movq %4,24(%2)
	jmp %5,%3
	movq 24(%2),%4
	movzbq 16(%2),%5
	addq $32,%2,%2
	subq %0,%5,%0
	jne .L357
.L358:
# ... } // while (Game())
	# Bilanz();
	# function call: Bilanz()
	subq $24,%2,%2
	ldzwq $Bilanz,%5
	movq %4,16(%2)
	jmp %5,%3
	movq 16(%2),%4
	addq $24,%2,%2
	ldzwq $0,%5
	movl %5,16(%1)
# return from function main:
.L355:
	addq $0,%1,%2
	movq 8(%2),%1
	movq 0(%2),%3
	jmp %3,%0
	.data
.L14:
	.string "Hatte eine ganze Zahl erwartet. Tschüss!\n"
	.data
.L41:
	.string "Gewünschte Bevölkerungszunahme im Bereich von "
	.data
.L42:
	.string " bis "
	.data
.L43:
	.string ": "
	.data
.L51:
	.string "\n"
	.data
.L61:
	.string "\nLetztes Wahlergebnis: "
	.data
.L62:
	.string "%\t               "
	.data
.L63:
	.string "Bevölkerung in Mio.: "
	.data
.L64:
	.string "\nBruttosozialprodukt in Mrd.: "
	.data
.L65:
	.string "\t               "
	.data
.L66:
	.string "Zustandsindex der Umwelt: "
	.data
.L67:
	.string "\nLebensqualitätsindex des Amts für Statistik: "
	.data
.L68:
	.string "\n"
	.data
.L76:
	.string " "
	.data
.L77:
	.string "\n"
	.data
.L103:
	.string "Sie haben "
	.data
.L104:
	.string " Aktionsmöglichkeiten.\n"
	.data
.L108:
	.string "> "
	.data
.L124:
	.string "Sie überschätzen Ihre Kräfte!\n"
	.data
.L169:
	.string "w\tWahlergebnisse\n"
	.data
.L170:
	.string "l\tLebensqualität\n"
	.data
.L171:
	.string "u\tUmweltzustand\n"
	.data
.L172:
	.string "p\tProduktion\n"
	.data
.L173:
	.string "b\tBevölkerung\n"
	.data
.L177:
	.string "Sanierung         s\n"
	.data
.L178:
	.string "Produktion        p\n"
	.data
.L179:
	.string "Aufklärung        a\n"
	.data
.L180:
	.string "Lebensqualität    l\n"
	.data
.L181:
	.string "Vermehrungsrate   v\n"
	.data
.L182:
	.string "Aktionsende       x\n"
	.data
.L183:
	.string "News              n\n"
	.data
.L187:
	.string "Unbekannte Aktionsmöglichkeit\n"
	.data
.L199:
	.string "Aufgrund des heftigen Widerstandes bricht die gegnerische\n"
	.data
.L200:
	.string "Offensive zusammen. Der UN-Generalsekretär vermittelt den\n"
	.data
.L201:
	.string "Abschluss eines Friedensvertrages.\n"
	.data
.L202:
	.string "Die Kämpfe fordern schwere Verluste.\n"
	.data
.L207:
	.string "Unzufriedene Massen demonstrieren in der Hauptstadt.\n"
	.data
.L211:
	.string "Sie entgehen nur knapp einem Anschlag auf Ihr Leben.\n"
	.data
.L220:
	.string "Einer Ihrer Minister ist in einen Skandal verwickelt\n"
	.data
.L221:
	.string "und tritt zurück.\n"
	.data
.L222:
	.string "Der Landwirtschaftsminister wird der Korruption überführt\n"
	.data
.L223:
	.string "und gesteht die Fälschung von Statistiken und "
	.data
.L224:
	.string "Hinterziehung\n der Sanierungsgelder.\n"
	.data
.L230:
	.string "Auf einer Mülldeponie werden hochgiftige Substanzen frei,\n"
	.data
.L231:
	.string "die dort illegal lagerten.\n"
	.data
.L237:
	.string "Bei einem Unfall in einem Kernkraftwerk werden hochaktive "
	.data
.L238:
	.string "Substanzen frei.\n"
	.data
.L247:
	.string "Eine bislang unbekannte Krankheit breitet sich epidemisch aus.\n"
	.data
.L253:
	.string "Einer Ihrer Nachbarstaaten erklärt Ihnen den Krieg.\n"
	.data
.L259:
	.string "Es lang dem Feind, die Front zu durchbrechen und ein "
	.data
.L260:
	.string "Industriezentrum einzunehmen.\n"
	.data
.L262:
	.string "     *** O E K O P O L Y ***\n\n"
	.data
.L263:
	.string "Sie können eines der folgenden Länder regieren:\n"
	.data
.L264:
	.string "\tTechnetia    t\n"
	.data
.L265:
	.string "\tKybernetien  k\n"
	.data
.L266:
	.string "\tAgrien       a\n"
	.data
.L267:
	.string "\tArminien     m\n"
	.data
.L268:
	.string "\tArmageddon   g\n"
	.data
.L269:
	.string "\tRandomia     r\n"
	.data
.L273:
	.string "Kennbuchstabe: "
	.data
.L292:
	.string "Armaggedon befindet sich im Kriegszustand.\n"
	.data
.L299:
	.string "Unbekannter Kennbuchstabe!\n"
	.data
.L304:
	.string "\n\n"
	.data
.L305:
	.string "... und das meint der Fachmann zu Ihrer Leistung:\n"
	.data
.L309:
	.string "Sie werden in die Geschichte eingehen -- "
	.data
.L310:
	.string "als 5. Apokalyptischer Reiter...\n"
	.data
.L314:
	.string "Für wen arbeiten Sie eigentlich?\n"
	.data
.L318:
	.string "Haben Sie einen Würfel oder eine Münze benutzt?\n"
	.data
.L322:
	.string "Sie zeigen vielversprechende Ansätze. Mehr üben!\n"
	.data
.L326:
	.string "Schade, dass Sie kein Politiker sind.\n"
	.data
.L327:
	.string "Sind Sie ein Genie, oder haben Sie voher geübt?!\n"
	.data
.L335:
	.string "Sie werden nicht mehr wiedergewählt.\n"
	.data
.L336:
	.string "Es gelingt Ihnen nicht mehr, vor der Revolution ins Ausland "
	.data
.L337:
	.string "zu flüchten.\n"
	.data
.L338:
	.string "Ihre Leiche wird in die Kanalisation geworfen.\n"
	.data
.L342:
	.string "Aufgrund Ihrer Erfolge werden Sie zum Präsidenten auf "
	.data
.L343:
	.string "Lebenszeit ernannt.\n"
	.data
.L347:
	.string "Trotz Ihrer verzweifelten Anstrengungen leben nur noch "
	.data
.L348:
	.string "zwei Einwohner,\ndie Sie mit vereinten Kräften "
	.data
.L349:
	.string "über die Grenze befördern.\n"
	.data
.L353:
	.string "Aufgrund Ihres hohen Alters lehnen Sie eine weitere "
	.data
.L354:
	.string "Kandidatur ab.\n"
	.bss
	.align 4
sani:
	.globl sani
	.space 4
	.align 4
prodi:
	.globl prodi
	.space 4
	.align 4
umwi:
	.globl umwi
	.space 4
	.align 4
aufi:
	.globl aufi
	.space 4
	.align 4
lebi:
	.globl lebi
	.space 4
	.align 4
veri:
	.globl veri
	.space 4
	.align 4
bevi:
	.globl bevi
	.space 4
	.align 4
poli:
	.globl poli
	.space 4
	.align 4
aktionen:
	.globl aktionen
	.space 4
	.align 4
runden:
	.globl runden
	.space 4
	.align 4
gift:
	.globl gift
	.space 4
	.align 1
krieg:
	.globl krieg
	.space 1
	.align 4
hist_ring:
	.globl hist_ring
	.space 100
	.data
	.align 4
gruen:
	.globl gruen
	.long 0
	.align 4
rot:
	.globl rot
	.long 1
	.align 4
blau:
	.globl blau
	.long 2
	.align 4
schwarz:
	.globl schwarz
	.long 3
	.align 8
sanierung:
	.globl sanierung
	.long 0
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.long 0
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 4
	.long 4
	.long 4
	.long 5
	.long 5
	.long 5
	.long 6
	.long 6
	.long 7
	.long 7
	.long 8
	.long 9
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.align 8
produktion:
	.globl produktion
	.long 0
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 1
	.long 0
	.long -1
	.long -2
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 4
	.long 4
	.long 5
	.long 5
	.long 6
	.long 6
	.long 7
	.long 7
	.long 8
	.long 9
	.long 10
	.long 12
	.long 14
	.long 18
	.long 22
	.long 0
	.long 0
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 4
	.long 4
	.long 4
	.long 5
	.long 5
	.long 5
	.long 6
	.long 6
	.long 6
	.long 7
	.long 7
	.long 8
	.long 8
	.long 9
	.long 9
	.long 10
	.long 10
	.long 11
	.long 11
	.align 8
umweltbelastung:
	.globl umweltbelastung
	.long 0
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.long 5
	.long 5
	.long 5
	.long 2
	.long 1
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 4
	.long 4
	.long 5
	.long 5
	.long 6
	.long 7
	.long 8
	.long 10
	.long 12
	.long 14
	.long 18
	.long 25
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.align 8
aufklaerung:
	.globl aufklaerung
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 1
	.long 1
	.long 2
	.long 2
	.long 3
	.long 3
	.long 4
	.long 4
	.long 5
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long -2
	.long -2
	.long -2
	.long -2
	.long -2
	.long -1
	.long -1
	.long -1
	.long -1
	.long 0
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.long 4
	.long 4
	.long 4
	.long 4
	.long 5
	.long 5
	.long 6
	.long 6
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.align 4
lebensqualitaet:
	.globl lebensqualitaet
	.long -10
	.long -8
	.long -6
	.long -3
	.long -2
	.long -1
	.long -1
	.long -1
	.long -1
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 4
	.long 4
	.long 5
	.long 0
	.long 1
	.long 1
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 2
	.long 1
	.long 1
	.long 0
	.long 0
	.long -1
	.long -1
	.long -1
	.long -1
	.long -1
	.long -2
	.long -2
	.long -2
	.long -1
	.long -1
	.long -1
	.long 0
	.long -15
	.long -8
	.long -6
	.long -4
	.long -3
	.long -2
	.long -1
	.long 0
	.long 1
	.long 2
	.long 2
	.long 2
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 4
	.long 4
	.long 4
	.long 4
	.long 5
	.long 5
	.long 5
	.align 8
vermehrungsrate:
	.globl vermehrungsrate
	.long 0
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.long 0
	.long -4
	.long -4
	.long -3
	.long -3
	.long -3
	.long -2
	.long -2
	.long -2
	.long -2
	.long -1
	.long -1
	.long -1
	.long -1
	.long -1
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.align 8
bevoelkerung:
	.globl bevoelkerung
	.long 0
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 4
	.long 4
	.long 4
	.long 4
	.long 5
	.long 5
	.long 6
	.long 7
	.long 8
	.long 10
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.long 4
	.long 4
	.long 4
	.long 4
	.long 5
	.long 5
	.long 5
	.long 5
	.long 6
	.long 6
	.long 6
	.long 6
	.long 7
	.long 7
	.long 7
	.long 7
	.long 8
	.long 8
	.long 8
	.long 8
	.long 9
	.long 9
	.long 9
	.align 8
politik:
	.globl politik
	.long 0
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.long 0
	.long 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.quad 0
	.long -10
	.long -9
	.long -8
	.long -7
	.long -6
	.long -5
	.long -4
	.long -3
	.long -2
	.long -1
	.long 0
	.long 1
	.long 2
	.long 3
	.long 4
	.long 5
	.long 6
	.long 7
	.long 8
	.long 9
	.long 10
	.long 11
	.long 12
	.long 13
	.long 14
	.long 15
	.long 16
	.long 17
	.long 18
	.long 19
	.long 20
	.long 21
	.long 22
	.long 23
	.long 24
	.long 25
	.long 26
	.long 27
	.long 28
	.long 29
	.long 30
	.long 31
	.long 32
	.long 33
	.long 34
	.long 35
	.long 36
	.long 37
	.long -1
	.long -1
	.long -1
	.long -1
	.long -1
	.long -1
	.long -1
	.long -1
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.long 3
	.align 4
pol:
	.globl pol
	.long 0
	.align 4
leb:
	.globl leb
	.long 1
	.align 4
umw:
	.globl umw
	.long 2
	.align 4
prod:
	.globl prod
	.long 3
	.align 4
bev:
	.globl bev
	.long 4
	.align 4
hist_length:
	.globl hist_length
	.long 5
	.align 4
hist_write:
	.globl hist_write
	.long 0
	.align 4
hist_written:
	.globl hist_written
	.long 0
