#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "codegen.h"
#include "expr.h"
#include "fixups.h"
#include "instr.h"
#include "lexer.h"
#include "msg.h"
#include "stop.h"
#include "symtab.h"

static size_t seg_;
static uint64_t addr_;
static FILE *out_;

enum ErrorCode {
    U8_OUT_OF_RANGE,
    S8_OUT_OF_RANGE,
    U16_OUT_OF_RANGE,
    S16_OUT_OF_RANGE,
    S24_REL_JMP_OUT_OF_RANGE,
    EXPECTED_MULT_OF_4,
    EXPECTED_ABS_EXPR,
};

static const char *errorStr[] = {
    // U8_OUT_OF_RANGE,
    "%" PRIu64 " out of range [0, 2^8-1]",

    // S8_OUT_OF_RANGE,
    "%" PRId64 " out of range [-2^7, 2^7-1]",

    // U16_OUT_OF_RANGE,
    "%" PRIu64 " out of range [0, 2^16-1]",

    // S16_OUT_OF_RANGE,
    "%" PRId64 " out of range [-2^15, 2^15-1]",

    // S24_REL_JMP_OUT_OF_RANGE,
    "offset '%" PRId64 "' for relative jump is out of range [-2^25, -2^25-1]",

    // EXPECTED_MULT_OF_4,
    "offset '%" PRId64 "' for relative jump is not a multiple of 4",

    // EXPECTED_ABS_EXPR
    "absolute expression expected",
};

static void
codegenError(const struct TokenLoc *loc, enum ErrorCode errorCode, uint64_t v)
{
    errorMsg(loc, errorStr[errorCode], v);
    stop(1);
}

enum Key {
    I1 = 0,
    R1 = 0,
    R2 = 1,
    R3 = 2,

    DIS = 0,
    BAS = 1, 
    IDX = 2,
    SRC = 4, 
    DST = 4, 
};

static uint32_t
imm8u(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    uint64_t v = extractValue(seg_, expr, addr_, offset, 1, FIXUP_ABSOLUTE);
    if (v >= (1<<8)) {
	codegenError(&instr->operand->u.expr[key]->loc, U8_OUT_OF_RANGE, v);
	stop(1);
    }
    return v & 0xFF;
}

static uint32_t
imm8s(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    int64_t v = extractValue(seg_, expr, addr_, offset, 1, FIXUP_ABSOLUTE);
    if (v < -(1<<7) || v >= (1<<7)) {
	codegenError(&instr->operand->u.expr[key]->loc, S8_OUT_OF_RANGE,
		     (uint64_t) v);
	stop(1);
    }
    return v & 0xFF;
}

static uint32_t
imm16u(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    uint64_t v = extractValue(seg_, expr, addr_, offset, 2, FIXUP_ABSOLUTE);
    if (v >= (1<<16)) {
	codegenError(&instr->operand->u.expr[key]->loc, U16_OUT_OF_RANGE, v);
	stop(1);
    }
    return v & 0xFFFF;
}

static uint32_t
imm16s(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    int64_t v = extractValue(seg_, expr, addr_, offset, 2, FIXUP_ABSOLUTE);
    if (v < -(1<<15) || v >= (1<<15)) {
	codegenError(&instr->operand->u.expr[key]->loc, S16_OUT_OF_RANGE,
		     (uint64_t) v);
	stop(1);
    }
    return v & 0xFFFF;
}

static uint32_t
relJmp24s(size_t offset, struct Instr *instr, enum Key key)
{
    struct Expr *expr = instr->operand->u.expr[key];

    fixExpressionType(expr);
    int64_t v = extractValue(seg_, expr, addr_, offset, 3, FIXUP_RELATIVE);
    if (expr->type != UNKNOWN && expr->type != ABSOLUTE) {
	v = v - instr->addr;
    }

    if (expr->type != UNKNOWN && v % 4) {
	codegenError(&instr->operand->u.expr[key]->loc, EXPECTED_MULT_OF_4, v);
	stop(1);
    }

    v /= 4;

    if (v < -(1<<23) || v >= (1<<23)) {
	codegenError(&instr->operand->u.expr[key]->loc,
		     S24_REL_JMP_OUT_OF_RANGE, (uint64_t) v*4);
	stop(1);
    }
    return v & 0xFFFFFF;
}

static uint32_t
encodeInstr(struct Instr *instr)
{
    enum TokenKind mnem = instr->mnemonic;
    enum InstrFmt fmt = instr->operand ? instr->operand->fmt : FMT_;

    uint32_t code = 0;

    switch (fmt) {
	case FMT_:
	    switch (mnem) {
		case NOP:
		    code = 0xFF;
		    code <<= 24;
		    break;
		default:
		    break;
	    }
	    break;
	case FMT_I:
	    switch (mnem) {
		case HALT:
		    code = 0x09;
		    code = code << 8 | imm8u(1, instr, I1);
		    code <<= 16;
		    break;
		case JA:
		case JNBE:
		    code = 0x4B;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JAE:
		case JNB:
		    code = 0x49;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JNAE:
		case JB:
		    code = 0x48;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JNA:
		case JBE:
		    code = 0x4A;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JE:
		case JZ:
		    code = 0x42;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JG:
		case JNLE:
		    code = 0x47;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JGE:
		case JNL:
		    code = 0x45;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JL:
		case JNGE:
		    code = 0x44;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JLE:
		case JNG:
		    code = 0x46;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JMP:
		    code = 0x41;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case JNE:
		case JNZ:
		    code = 0x43;
		    code = code << 24 |  relJmp24s(1, instr, I1);
		    break;
		case PUTC:
		    code = 0x69;
		    code = code << 8 | imm8u(1, instr, I1);
		    code <<= 16;
		    break;
		default:
		    break;
	    }
	    break;

	case FMT_R:
	    switch (mnem) {
		case GETC:
		    code = 0x60;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		case HALT:
		    code = 0x01;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		case PUTC:
		    code = 0x61;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		default:
		    break;
	    }
	    code <<= 16;
	    break;

	case FMT_IR:
	    switch (mnem) {
		case LDSWQ:
		    code = 0x57;
		    code = code << 16 | imm16s(1, instr, I1);
		    code = code << 8 | imm8u(3, instr, R2);
		    break;
		case LDZWQ:
		    code = 0x56;
		    code = code << 16 | imm16u(1, instr, I1);
		    code = code << 8 | imm8u(3, instr, R2);
		    break;
		case SHLDWQ:
		    code = 0x5D;
		    code = code << 16 | imm16u(1, instr, I1);
		    code = code << 8 | imm8u(3, instr, R2);
		    break;
		default:
		    break;
	    }
	    break;

	case FMT_RR:
	    switch (mnem) {
		case JMP:
		    code = 0x40;
		    break;
		case NOTQ:
		    code = 0x5E;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, R1);
	    code = code << 8 | imm8u(2, instr, R2);
	    code <<= 8;
	    break;

	case FMT_RRR:
	    switch (mnem) {
		case ADDQ:
		    code = 0x30;
		    break;
		case ANDQ:
		    code = 0x51;
		    break;
		case DIVQ:
		    code = 0x33;
		    break;
		case IDIVQ:
		    code = 0x35;
		    break;
		case IMULQ:
		    code = 0x34;
		    break;
		case MULQ:
		    code = 0x32;
		    break;
		case ORQ:
		    code = 0x50;
		    break;
		case SALQ:
		case SHLQ:
		    code = 0x52;
		    break;
		case SARQ:
		    code = 0x54;
		    break;
		case SHRQ:
		    code = 0x53;
		    break;
		case SUBQ:
		    code = 0x31;
		    break;
		case TRAP:
		    code = 0x02;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, R1);
	    code = code << 8 | imm8u(2, instr, R2);
	    code = code << 8 | imm8u(3, instr, R3);
	    break;

	case FMT_IRR:
	    switch (mnem) {
		case ADDQ:
		    code = 0x38;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		case DIVQ:
		    code = 0x3B;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		case IDIVQ:
		    code = 0x3D;
		    code = code << 8 | imm8s(1, instr, I1);
		    break;
		case IMULQ:
		    code = 0x3C;
		    code = code << 8 | imm8s(1, instr, I1);
		    break;
		case MULQ:
		    code = 0x3A;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		case SALQ:
		case SHLQ:
		    code = 0x5A;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		case SARQ:
		    code = 0x5C;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		case SHRQ:
		    code = 0x5B;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		case SUBQ:
		    code = 0x39;
		    code = code << 8 | imm8u(1, instr, I1);
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(2, instr, R2);
	    code = code << 8 | imm8u(3, instr, R3);
	    break;

	case FMT_IMR_R:
	    switch (mnem) {
		case MOVSBQ:
		    code = 0x1F;
		    break;
		case MOVZBQ:
		    code = 0x1B;
		    break;
		case MOVSLQ:
		    code = 0x1D;
		    break;
		case MOVZLQ:
		    code = 0x19;
		    break;
		case MOVSWQ:
		    code = 0x1E;
		    break;
		case MOVZWQ:
		    code = 0x1A;
		    break;
		case MOVQ:
		    code = 0x18;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8s(1, instr, DIS);
	    code = code << 8 | imm8u(2, instr, BAS);
	    code = code << 8 | imm8u(3, instr, DST);
	    break;
	case FMT_MRR1_R:
	    switch (mnem) {
		case MOVSBQ:
		    code = 0x17;
		    break;
		case MOVZBQ:
		    code = 0x13;
		    break;
		case MOVSLQ:
		    code = 0x15;
		    break;
		case MOVZLQ:
		    code = 0x11;
		    break;
		case MOVSWQ:
		    code = 0x16;
		    break;
		case MOVZWQ:
		    code = 0x12;
		    break;
		case MOVQ:
		    code = 0x10;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, BAS);
	    code = code << 8 | imm8u(2, instr, IDX);
	    code = code << 8 | imm8u(3, instr, DST);
	    break;
	case FMT_MRR2_R:
	    switch (mnem) {
		case MOVSBQ:
		    code = 0x87;
		    break;
		case MOVZBQ:
		    code = 0x83;
		    break;
		case MOVSLQ:
		    code = 0x85;
		    break;
		case MOVZLQ:
		    code = 0x81;
		    break;
		case MOVSWQ:
		    code = 0x86;
		    break;
		case MOVZWQ:
		    code = 0x82;
		    break;
		case MOVQ:
		    code = 0x80;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, BAS);
	    code = code << 8 | imm8u(2, instr, IDX);
	    code = code << 8 | imm8u(3, instr, DST);
	    break;
	case FMT_MRR4_R:
	    switch (mnem) {
		case MOVSBQ:
		    code = 0xA7;
		    break;
		case MOVZBQ:
		    code = 0xA3;
		    break;
		case MOVSLQ:
		    code = 0xA5;
		    break;
		case MOVZLQ:
		    code = 0xA1;
		    break;
		case MOVSWQ:
		    code = 0xA6;
		    break;
		case MOVZWQ:
		    code = 0xA2;
		    break;
		case MOVQ:
		    code = 0xA0;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, BAS);
	    code = code << 8 | imm8u(2, instr, IDX);
	    code = code << 8 | imm8u(3, instr, DST);
	    break;
	case FMT_MRR8_R:
	    switch (mnem) {
		case MOVSBQ:
		    code = 0xC7;
		    break;
		case MOVZBQ:
		    code = 0xC3;
		    break;
		case MOVSLQ:
		    code = 0xC5;
		    break;
		case MOVZLQ:
		    code = 0xC1;
		    break;
		case MOVSWQ:
		    code = 0xC6;
		    break;
		case MOVZWQ:
		    code = 0xC2;
		    break;
		case MOVQ:
		    code = 0xC0;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, BAS);
	    code = code << 8 | imm8u(2, instr, IDX);
	    code = code << 8 | imm8u(3, instr, DST);
	    break;
	case FMT_R_IMR:
	    switch (mnem) {
		case MOVB:
		    code = 0x2B;
		    break;
		case MOVL:
		    code = 0x29;
		    break;
		case MOVW:
		    code = 0x2A;
		    break;
		case MOVQ:
		    code = 0x28;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, SRC);
	    code = code << 8 | imm8s(2, instr, DIS);
	    code = code << 8 | imm8u(3, instr, BAS);
	    break;
	case FMT_R_MRR1:
	    switch (mnem) {
		case MOVB:
		    code = 0x23;
		    break;
		case MOVL:
		    code = 0x21;
		    break;
		case MOVW:
		    code = 0x22;
		    break;
		case MOVQ:
		    code = 0x20;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, SRC);
	    code = code << 8 | imm8u(2, instr, BAS);
	    code = code << 8 | imm8u(3, instr, IDX);
	    break;
	case FMT_R_MRR2:
	    switch (mnem) {
		case MOVB:
		    code = 0x93;
		    break;
		case MOVL:
		    code = 0x91;
		    break;
		case MOVW:
		    code = 0x92;
		    break;
		case MOVQ:
		    code = 0x90;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, SRC);
	    code = code << 8 | imm8u(2, instr, BAS);
	    code = code << 8 | imm8u(3, instr, IDX);
	    break;
	case FMT_R_MRR4:
	    switch (mnem) {
		case MOVB:
		    code = 0xB3;
		    break;
		case MOVL:
		    code = 0xB1;
		    break;
		case MOVW:
		    code = 0xB2;
		    break;
		case MOVQ:
		    code = 0xB0;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, SRC);
	    code = code << 8 | imm8u(2, instr, BAS);
	    code = code << 8 | imm8u(3, instr, IDX);
	    break;
	case FMT_R_MRR8:
	    switch (mnem) {
		case MOVB:
		    code = 0xD3;
		    break;
		case MOVL:
		    code = 0xD1;
		    break;
		case MOVW:
		    code = 0xD2;
		    break;
		case MOVQ:
		    code = 0xD0;
		    break;
		default:
		    break;
	    }
	    code = code << 8 | imm8u(1, instr, SRC);
	    code = code << 8 | imm8u(2, instr, BAS);
	    code = code << 8 | imm8u(3, instr, IDX);
	    break;

	default:
	    fprintf(stderr, "internal error: format not handled\n");
	    fprintf(stderr, "mnemonic: %s\n", tokenKindStr(mnem));
	    fprintf(stderr, "instruction format: %d\n", fmt);
	    stop(2);
    }
    if (code >> 24 == 0) {
	fprintf(stderr, "internal error: opcode not handled\n");
	fprintf(stderr, "mnemonic: %s\n", tokenKindStr(mnem));
	fprintf(stderr, "instruction format: %d\n", fmt);
	stop(2);
    }
    return code;
}


static void
encode(size_t seg, uint64_t addrOffset, struct Instr *instr)
{
    seg_ = seg;
    addr_ = addrOffset + instr->addr;

    fprintf(out_, "0x%016" PRIx64 ":", addrOffset + instr->addr);
    if (instr->mnemonic == DOT_STRING) {
	const char *str = instr->operand->u.str[0];
	do {
	    fprintf(out_, " %02x", *str & 0xFF);
	} while (*str++);
    } else if (instr->mnemonic == DOT_SPACE) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(expr);
	if (expr->type == UNKNOWN) {
	    codegenError(&expr->loc, EXPECTED_ABS_EXPR, 0);
	}
	uint64_t v = evalExpression(expr);
	while (v--) {
	    fprintf(out_, " 00");
	}
    } else if (instr->mnemonic == DOT_BYTE) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(expr);
	uint64_t v = extractValue(seg_, expr, addr_, 0, 1, FIXUP_ABSOLUTE);
	fprintf(out_, " %02" PRIx64 , v & 0xFF);
    } else if (instr->mnemonic == DOT_WORD) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(instr->operand->u.expr[0]);
	uint64_t v = extractValue(seg_, expr, addr_, 0, 2, FIXUP_ABSOLUTE);
	fprintf(out_, " %04" PRIx64 , v & 0xFFFF);
    } else if (instr->mnemonic == DOT_LONG) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(expr);
	uint64_t v = extractValue(seg_, expr, addr_, 0, 4, FIXUP_ABSOLUTE);
	fprintf(out_, " %08" PRIx64 , v & 0xFFFFFFFF);
    } else if (instr->mnemonic == DOT_QUAD) {
	struct Expr *expr = instr->operand->u.expr[0];
	fixExpressionType(expr);
	uint64_t v = extractValue(seg_, expr, addr_, 0, 8, FIXUP_ABSOLUTE);
	fprintf(out_, " %016" PRIx64 , v);
    } else {
    	union {
    	    uint32_t all;
    	    unsigned char byte[4];
    	} code;

    	code.all = encodeInstr(instr);
    	fprintf(out_, " %02x %02x %02x %02x", code.byte[3], code.byte[2],
    	        code.byte[1], code.byte[0]);
    }
    fprintf(out_, " # \t");
    fprintInstr(out_, instr);
    fprintf(out_, "\n");
}

static const char *segStr[] = {
    "TEXT",
    "DATA",
    "BSS",
};

void
codegen(FILE *out)
{
    out_ = out;
    fixSymtab();
    for (size_t seg = 0; seg < 2; ++seg) {
	if (! getSegmentSize(seg)) {
	    continue;
	}
	fprintf(out_, "#%s %lu\n", segStr[seg], getSegmentAlignment(seg));

	struct Instr *instr = segment[seg];

	while (instr) {
	    encode(seg, getSegmentOffset(seg), instr);
	    instr = instr->next;
	}
    }
    if (getSegmentSize(2)) {
	fprintf(out_, "#BSS %lu %lu\n", getSegmentAlignment(2),
		getSegmentSize(2));
    }
    fprintSymTab(out_);
    fprintFixupTab(out_);
}
