#include <stdio.h>

#include "lexer.h"
#include "parser.h"

int
main()
{
    if (! parse(stdin)) {
	printf("syntax error\n");
    }
}
