#ifndef ULMAS_LEXER_H
#define ULMAS_LEXER_H

#include <stddef.h>
#include <stdio.h>


enum TokenKind
{
    EOI = 0,
    EMPTY_LABEL,
    UNKNOWN_TOKEN,
    DECIMAL_LITERAL,
    OCTAL_LITERAL,
    HEXADECIMAL_LITERAL,
    CHARACTER_LITERAL,
    STRING_LITERAL,
    IDENT,
    EOL,
    PLUS,
    MINUS,
    ASTERISK,
    SLASH,
    PERCENT,
    COLON,
    COMMA,
    DOLLAR,
    LPAREN,
    LPAREN_MEM,
    RPAREN,
    W0,
    W1,
    W2,
    W3,

    ADDQ,
    ANDQ,
    DIVQ,
    GETC,
    HALT,
    IDIVQ,
    IMULQ,
    JA,
    JAE,
    JB,
    JBE,
    JE,
    JG,
    JGE,
    JL,
    JLE,
    JMP,
    JNA,
    JNAE,
    JNB,
    JNBE,
    JNE,
    JNG,
    JNGE,
    JNL,
    JNLE,
    JNZ,
    JZ,
    LDSWQ,
    LDZWQ,
    MOVB,
    MOVL,
    MOVQ,
    MOVSBQ,
    MOVSLQ,
    MOVSWQ,
    MOVW,
    MOVZBQ,
    MOVZLQ,
    MOVZWQ,
    MULQ,
    NOP,
    NOTQ,
    ORQ,
    PUTC,
    SALQ,
    SARQ,
    SHLDWQ,
    SHLQ,
    SHRQ,
    SUBQ,
    TRAP,

    DOT_ALIGN,
    DOT_BSS,
    DOT_BYTE,
    DOT_DATA,
    DOT_EQU,
    DOT_EQUIV,
    DOT_GLOBL,
    DOT_GLOBAL,
    DOT_LONG,
    DOT_QUAD,
    DOT_SET,
    DOT_SPACE,
    DOT_STRING,
    DOT_TEXT,
    DOT_WORD,
};

struct TokenPos
{
    size_t line, col;
};

struct TokenLoc
{
    struct TokenPos begin, end;
};

extern enum TokenKind tokenKind;
extern const char *tokenValue, *processedTokenValue;
extern struct TokenLoc tokenLoc;

enum TokenKind getToken();
const char *tokenKindStr(enum TokenKind tokenKind);
const char *mnemonicStr(enum TokenKind tokenKind);

const char *currentInputLine();
size_t currentInputLineNumber();

const char *previousInputLine();
size_t previousInputLineNumber();

void setLexerIn(FILE *in);

#endif // ULMAS_LEXER_H
