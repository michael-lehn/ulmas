#include <stdio.h>
#include <stdarg.h>


#include "msg.h"

enum Color {
    NORMAL,
    BOLD, // normal bold
    RED,
    BLUE,
    BOLD_RED,
    BOLD_BLUE,
};

static const char *colorStr[] = {
    "\033[0m",
    "\033[1;10m",
    "\033[0;31m",
    "\033[0;34m",
    "\033[1;31m",
    "\033[1;34m",
};

static void
setColor(enum Color color)
{
    fprintf(stderr, "%s", colorStr[color]);
}

// http://c-faq.com/varargs/handoff.html

static void
vmsg(const struct TokenLoc *loc, enum Color msgTypeColor, const char *msgType,
     const char *fmt, va_list argp)
{
    if (loc) {
	setColor(BOLD);
	fprintf(stderr, "%lu.%lu-%lu.%lu: ", loc->begin.line,
		loc->begin.col, loc->end.line, loc->end.col);
	setColor(NORMAL);
    }
   
    if (msgType) {
	setColor(msgTypeColor);
	fprintf(stderr, "%s: ", msgType);
	setColor(NORMAL);
    }

    if (fmt) {
	vfprintf(stderr, fmt, argp);
    }
    fprintf(stderr, "\n");

    if (loc) {
	if (currentInputLineNumber() == loc->begin.line) {
	    fprintf(stderr, "%s", currentInputLine());
	    for (size_t i = 1; i < loc->begin.col; ++i) {
		fprintf(stderr, " ");
	    }
	    fprintf(stderr, "\033[1;31m^\033[0;30m\n");
	} else if (previousInputLineNumber() == loc->begin.line) {
	    fprintf(stderr, "%s", previousInputLine());
	    for (size_t i = 1; i < loc->begin.col; ++i) {
		fprintf(stderr, " ");
	    }
	    fprintf(stderr, "\033[1;31m^\033[0;30m\n");
	} else {
	    /*
	    printf("token = %s\n", tokenKindStr(tokenKind));
	    printf("token = %s\n", tokenKindStr(tokenKind));
	    printf("currentInputLineNumber() = %lu\n",
		   currentInputLineNumber());
	    printf("loc->begin.line = %lu\n", loc->begin.line);
	    printCh();
	    */
	}
	setColor(NORMAL);
   }
}

void
errorMsg(const struct TokenLoc *loc, const char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vmsg(loc, BOLD_RED, "\xF0\x9F\x98\xA2 error", fmt, argp);
    va_end(argp);
}

void
warningMsg(const struct TokenLoc *loc, const char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vmsg(loc, BOLD_BLUE, "\xF0\x9F\x98\xAC warning", fmt, argp);
    va_end(argp);
}
