#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "fixups.h"
#include "instr.h"
#include "memregion.h"
#include "msg.h"
#include "stop.h"

struct Entry
{
    struct Entry *next;
    // where to fix
    size_t seg;
    uint64_t addr;
    size_t offset;
    size_t numBytes;

    // how to fix
    enum FixupType fixupType;
    const char *sym;
    bool addDispl;
    uint64_t displ;
};

static const char *segStr[] = {
    "[text]",
    "[data]",
    "[bss]",
};

static struct Entry *fixups;

void
fixupError(const struct Expr *expr)
{
    errorMsg(&expr->loc, "expected relocatable expression");
}

uint64_t
extractValue(size_t seg, const struct Expr *expr, uint64_t addr, size_t offset,
	     size_t numBytes, enum FixupType fixupType)
{
    uint64_t val = evalExpression(expr);

    if (expr->type == ABSOLUTE) {
	return val;
    }

    // expr->type != UNKNOWN means it is TEXT, DATA, or BSS
    if (fixupType == FIXUP_RELATIVE && expr->type == seg) {
	return val;
    }

    uint64_t val_ = val;
    if (expr->op == OP_W0) {
	expr = expr->u.child[0];
	fixupType = FIXUP_W0_;
	val_ = evalExpression(expr);
    } else if (expr->op == OP_W1) {
	expr = expr->u.child[0];
	fixupType = FIXUP_W1_;
	val_ = evalExpression(expr);
    } else if (expr->op == OP_W2) {
	expr = expr->u.child[0];
	fixupType = FIXUP_W2_;
	val_ = evalExpression(expr);
    } else if (expr->op == OP_W3) {
	expr = expr->u.child[0];
	fixupType = FIXUP_W3_;
	val_ = evalExpression(expr);
    }

    struct Entry *newEntry = alloc(FIXUP, sizeof(*newEntry));

    newEntry->seg = seg;
    newEntry->addr = addr;
    newEntry->offset = offset;
    newEntry->numBytes = numBytes;
    newEntry->fixupType = fixupType;
    newEntry->displ = val_;

    if (expr->type != UNKNOWN) {
	newEntry->sym = segStr[expr->type];
	newEntry->displ -= getSegmentOffset(expr->type);
	newEntry->addDispl = true;
    } else if (expr->op == SYM) {
	newEntry->sym = expr->u.sym;
	newEntry->addDispl = true;
    } else if (expr->op == ADD || expr->op == SUB) {
	newEntry->addDispl = expr->op == ADD;

	const struct Expr *left = expr->u.child[0];
	const struct Expr *right = expr->u.child[1];

	if (right->type == UNKNOWN) {
	    const struct Expr *tmp = left;
	    left = right;
	    right = tmp;
	}

	if (right->type != ABSOLUTE) {
	    fixupError(expr);
	}

	if (left->type == UNKNOWN) {
	    newEntry->sym = left->u.sym;
	} else {
	    newEntry->sym = segStr[left->type];
	    newEntry->displ -= getSegmentOffset(expr->type);
	}

    } else {
	fixupError(expr);
    }

    newEntry->next = fixups;
    fixups = newEntry;
    return val;
}

void
fprintFixupTab(FILE *out)
{
    if (! fixups) {
	return;
    }

    fprintf(out, "#FIXUPS\n");
    for (struct Entry *f = fixups; f; f = f->next) {
	switch (f->seg) {
	    case TEXT:
		fprintf(out, "text ");
		break;

	    case DATA:
		fprintf(out, "data ");
		break;

	    case BSS:
	    default:
		fprintf(stderr, "internal error: unexpected segment %zu",
			f->seg);
		stop(2);
	}

	fprintf(out, "0x%016" PRIx64 " ", f->addr - getSegmentOffset(f->seg));
	fprintf(out, "%zu ", f->offset);
	fprintf(out, "%zu ", f->numBytes);

	switch (f->fixupType) {
	    case FIXUP_ABSOLUTE:
		fprintf(out, "absolute ");
		break;

	    case FIXUP_RELATIVE:
		fprintf(out, "relative ");
		break;

	    case FIXUP_W0_:
		fprintf(out, "w0 ");
		break;

	    case FIXUP_W1_:
		fprintf(out, "w1 ");
		break;

	    case FIXUP_W2_:
		fprintf(out, "w2 ");
		break;

	    case FIXUP_W3_:
		fprintf(out, "w3 ");
		break;

	    default:
		fprintf(stderr, "internal error: unexpected fixup type %d",
			f->fixupType);
		stop(2);
	}
	fprintf(out, "%s", f->sym);
	if (f->displ) {
	    if (f->addDispl) {
		fprintf(out, "+");
	    } else {
		fprintf(out, "-");
	    }
	    fprintf(out, "%" PRIu64, f->displ);
	}
	fprintf(out, "\n");
    }
}
