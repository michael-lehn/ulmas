#include <stdlib.h>
#include <stdio.h>

#include "codegen.h"
#include "fixups.h"
#include "instr.h"
#include "lexer.h"
#include "parser.h"
#include "string.h"
#include "stop.h"
#include "symtab.h"

void
usage(const char *cmdname)
{
    fprintf(stderr, "Usage: %s [-o outfile] infile\n", cmdname);
    exit(1);
}

int
main(int argc, char *argv[])
{
    char *inputfile = 0;
    char *outputfile = 0;

    for (int i = 1; i < argc; ++i) {
	if (argv[i][0] == '-') {
	    if (argv[i][1] == 'o') {
		if (outputfile) {
		    usage(argv[0]);
		}
		if (i + 1 >= argc) {
		    usage(argv[0]);
		}
		++i;
		outputfile = argv[i];
	    } else {
		usage(argv[0]);
	    }
	} else {
	    if (inputfile) {
		usage(argv[0]);
	    }
	    inputfile = argv[i];
	}
    }

    FILE *in = stdin, *out = stdout;

    if (inputfile) {
	in = fopen(inputfile, "r");
	if (! in) {
	    fprintf(stderr, "%s: can not open for reading\n", inputfile);
	}
    }

    if (! outputfile) {
	outputfile = "a.out";
    }

    out = fopen(outputfile, "w");
    deleteOutputOnStop(out, outputfile);
    if (! out) {
	fprintf(stderr, "%s: can not open for writing\n", outputfile);
    }

    if (parse(in)) {
	codegen(out);
    }

    if (inputfile) {
	fclose(in);
    }
    fclose(out);
}
