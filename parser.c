#include <stdio.h>
#include <stdlib.h>

#include "codegen.h"
#include "expr.h"
#include "instr.h"
#include "lexer.h"
#include "msg.h"
#include "parser.h"
#include "stop.h"
#include "string.h"
#include "symtab.h"

#ifndef PARSER_BUFFER_SIZE
#define PARSER_BUFFER_SIZE 16
#endif


size_t errorCount;

enum ErrorCode {
    OP_NOT_IMPLEMENTED,
    EXPECTED_3ADDR_OPERAND,
    EXPECTED_2ADDR_OPERAND,
    EXPECTED_1ADDR_OPERAND,
    EXPECTED_2ADDR_OR_1ADDR_OPERAND,
    EXPECTED_FETCH_OPERAND,
    EXPECTED_STORE_OPERAND,
    EXPECTED_FETCH_OR_STORE_OPERAND,
    EXPECTED_SCALE_OPERAND,
    EXPECTED_REGISTER_OPERAND,
    EXPECTED_MEMORY_OPERAND,
    EXPECTED_EXPR_AFTER_DOLLAR,
    EXPECTED_TERM,
    EXPECTED_FACTOR_AFTER_UNARY_MINUS,
    EXPECTED_FACTOR_AFTER_UNARY_PLUS,
    EXPECTED_EXPRESSION,
    EXPECTED_LPAREN,
    EXPECTED_RPAREN,
    EXPECTED_MNEMONIC_OR_PSEUDOOP,
    EXPECTED_IDENT,
};

static const char *errorStr[] = {
    // OP_NOT_IMPLEMENTED
    "not your fault. Operation '%s' is currently not implemented",

    // EXPECTED_3ADDR_OPERAND
    "'%s' expects a 3 address operand",

    // EXPECTED_2ADDR_OPERAND
    "'%s' expects a 2 address operand",

    // EXPECTED_1ADDR_OPERAND
    "'%s' expects a 1 address operand",

    //EXPECTED_2ADDR_OR_1ADDR_OPERAND,
    "'%s' expects a 1 or 2 address operand",

    // EXPECTED_FETCH_OPERAND
    "'%s' expects fetch operand",

    // EXPECTED_STORE_OPERAND
    "'%s' expects store operand",

    // EXPECTED_FETCH_OR_STORE_OPERAND
    "'%s' expects fetch or store operand",

    // EXPECTED_SCALE_OPERAND
    "absoult expression (1, 2, 4, or 8) for scale expected",

    // EXPECTED_REGISTER_OPERAND
    "register operand expected",

    // EXPECTED_MEMORY_OPERAND
    "memory operand expected",

    // EXPECTED_EXPR_AFTER_DOLLAR
    "expression expected after '$'",

    // EXPECTED_TERM
    "term expected",

    // EXPECTED_FACTOR_AFTER_UNARY_MINUS
    "factor expected after unary '-'",

    // EXPECTED_FACTOR_AFTER_UNARY_PLUS
    "factor expected after unary '+'",

    // EXPECTED_EXPRESSION
    "expression expected",

    // EXPECTED_LPAREN,
    "'(' expected",

    // EXPECTED_RPAREN
    "')' expected",

    // EXPECTED_MNEMONIC_OR_PSEUDOOP
    "expected mnemonic or directive. Got %s",

    // EXPECTED_IDENT
    "expected identifier",
};

enum WarningCode {
    OVERFLOW_DECIMAL_LITERAL,
    OVERFLOW_OCTAL_LITERAL,
    OVERFLOW_HEXADECIMAL_LITERAL,
};

static const char *warningStr[] = {
    // OVERFLOW_DECIMAL_LITERAL
    "decimal literal can not be represented with 64 bits.",

    // OVERFLOW_OCTAL_LITERAL
    "octal literal can not be represented with 64 bits.",

    // OVERFLOW_HEXADECIMAL_LITERAL
    "hexadecimal literal can not be represented with 64 bits.",
};

void
stopLexerAtNextNewline()
{
    // will also stop when Eof is reached
    while (tokenKind != EOL) {
	getToken();
	if (tokenKind == EOI) {
	    return;
	}
    }
}

static void
parseError(const struct TokenLoc *loc, enum ErrorCode errorCode,
	   const enum TokenKind *tokenKind)
{
    const char *opStr = tokenKind
	? *tokenKind >= ADDQ
	    ? mnemonicStr(*tokenKind)
	    : tokenKindStr(*tokenKind)
	: 0;
    errorMsg(loc, errorStr[errorCode], opStr);
    stopLexerAtNextNewline();
    ++errorCount;
}

static bool
expected(enum TokenKind t)
{
    if (t != tokenKind) {
	errorMsg(&tokenLoc, "expected '%s' got '%s'", tokenKindStr(t),
		 tokenKindStr(tokenKind));
	stopLexerAtNextNewline();
	++errorCount;
	return false;
    }
    getToken();
    return true;
}

static void
parseWarning(const struct TokenLoc *loc, enum WarningCode warningCode,
	     const enum TokenKind *tokenKind)
{
    const char *opStr = tokenKind ? tokenKindStr(*tokenKind) : 0;
    warningMsg(loc, warningStr[warningCode], opStr);
}

double
decToDouble(const char *str)
{
    double v = 0;
    while (*str) {
	v *= 10;
	v += *str++ - '0';
    }
    return v;
}

static bool
octStr_to_uint64_t(const char *s, uint64_t *v)
{
    bool noOverflow = true;
    uint64_t value = 0;
    while (*s) {
	uint64_t d = *s++ - '0';
	if (value > (UINT64_MAX - d)/8) {
	    noOverflow = false;
	}
	value = value*8 + d;
    }
    *v = value;
    return noOverflow;
}

static bool
decStr_to_uint64_t(const char *s, uint64_t *v)
{
    bool noOverflow = true;
    uint64_t value = 0;
    while (*s) {
	uint64_t d = *s++ - '0';
	if (value > (UINT64_MAX - d)/10) {
	    noOverflow = false;
	}
	value = value*10 + d;
    }
    *v = value;
    return noOverflow;
}

static bool
hexStr_to_uint64_t(const char *s, uint64_t *v)
{
    bool noOverflow = true;
    uint64_t value = 0;
    // skip prefix 0x or 0X
    s += 2;
    while (*s) {
	char ch = *s++;

	uint64_t d = 0;
	if (ch >= '0' && ch <= '9') {
	    d = ch - '0';
	} else if (ch >= 'a' && ch <= 'f') {
	    d = 10 + ch - 'a';
	} else if (ch >= 'A' && ch <= 'F') {
	    d = 10 + ch - 'A';
	} else {
	    fprintf(stderr, "charcater '%c' (int value = %d) is not a hex"
		    " digit", ch, ch);
	    stop(2);
	}
	if (value > (UINT64_MAX - d)/10) {
	    noOverflow = false;
	}
	value = value*16 + d;
    }
    *v = value;
    return noOverflow;
}

struct Instr *parseLabelledOp();
struct Instr *parseDirective();
struct Instr *parseInstruction();
struct InstrOperand *parseOperands_I();
struct InstrOperand *parseOperands_R();
struct InstrOperand *parseOperands_RR();
struct InstrOperand *parseOperands_IR();
struct InstrOperand *parseOperands_IRR();
struct InstrOperand *parseOperands_RRR();
struct InstrOperand *parseOperands_MR();
struct InstrOperand *parseOperands_RM();
struct Expr *parseWordOp();
struct Expr *parseImmediate();
struct Expr *parseRegister();
struct Expr *parseExpression();
struct Expr *parseTerm();
struct Expr *parseFactor();

bool
parse(FILE *in)
{
    setLexerIn(in);
    errorCount = 0;
    getToken();
    while (tokenKind != EOI) {
	parseLabelledOp();
	expected(EOL);

    }
    return errorCount == 0;
}

struct Instr *
parseLabelledOp()
{
    if (tokenKind != IDENT && tokenKind != EMPTY_LABEL) {
	return 0;
    } else if (tokenKind == IDENT) {
	const char *ident = addString(tokenValue);
	setLabel(&tokenLoc, ident);
    }
    getToken();
    if (tokenKind == COLON) {
	getToken();
    }
    if (tokenKind == EOL) {
	return 0;
    }

    struct Instr *instr;

    if ((instr = parseDirective())) {
	return instr;
    } else if ((instr = parseInstruction())) {
	return instr;
    }

    return 0;
}

struct Instr *
parseDirective()
{
    enum TokenKind op = tokenKind;

    if (op < DOT_ALIGN) {
	return 0;
    }

    getToken();

    const char *pstr = 0, *str = 0;
    struct Expr *expr0 = 0;

    switch (op) {
	case DOT_SET:
	case DOT_EQU:
	case DOT_EQUIV:
	    if (tokenKind != IDENT) {
		parseError(&tokenLoc, EXPECTED_IDENT, 0);	
	    }
	    str = addString(tokenValue);
	    getToken();
	    expected(COMMA);
	    if ((expr0 = parseExpression())) {
		addSym(str, expr0);
		return 0;
	    }
	    parseError(&tokenLoc, EXPECTED_EXPRESSION, 0);	
	    return 0;

	case DOT_ALIGN:
	    if ((expr0 = parseExpression())) {
		uint64_t v= evalExpression(expr0);
		setAlignment(v);
		return 0;
	    }
	    parseError(&tokenLoc, EXPECTED_EXPRESSION, 0);	
	    return 0;


	case DOT_SPACE:
	    if ((expr0 = parseExpression())) {
		return makeInstr(op, make1AddrOperand(FMT_I, expr0));
	    }
	    parseError(&tokenLoc, EXPECTED_EXPRESSION, 0);	
	    return 0;

	case DOT_STRING:
	    if (tokenKind == STRING_LITERAL) {
		pstr = addString(processedTokenValue);
		str = addString(tokenValue);
		getToken();
		return makeStringPseudoOp(pstr, str);
	    }
	    expected(STRING_LITERAL);
	    return 0;

	case DOT_TEXT:
	    assembleText();
	    return 0;

	case DOT_DATA:
	    assembleData();
	    return 0;

	case DOT_BSS:
	    assembleBss();
	    return 0;

	case DOT_BYTE:
	    if ((expr0 = parseExpression())) {
		return makeInstr(op, make1AddrOperand(FMT_I, expr0));
	    }
	    parseError(&tokenLoc, EXPECTED_EXPRESSION, 0);	
	    return 0;

	case DOT_WORD:
	    if ((expr0 = parseExpression())) {
		return makeInstr(op, make1AddrOperand(FMT_I, expr0));
	    }
	    parseError(&tokenLoc, EXPECTED_EXPRESSION, 0);	
	    return 0;

	case DOT_LONG:
	    if ((expr0 = parseExpression())) {
		return makeInstr(op, make1AddrOperand(FMT_I, expr0));
	    }
	    parseError(&tokenLoc, EXPECTED_EXPRESSION, 0);	
	    return 0;

	case DOT_QUAD:
	    if ((expr0 = parseExpression())) {
		return makeInstr(op, make1AddrOperand(FMT_I, expr0));
	    }
	    parseError(&tokenLoc, EXPECTED_EXPRESSION, 0);	
	    return 0;

	case DOT_GLOBL:
	case DOT_GLOBAL:
	    if (tokenKind == IDENT) {
		globalSym(addString(tokenValue));
		getToken();
		return 0;
	    }
	    parseError(&tokenLoc, EXPECTED_IDENT, 0);	
	    return 0;

	default:
	    printf("internal error: not handled tokenKind = %d (%s)\n",
		   op, tokenKindStr(op));
	    stop(2);
	    ;
    }
    return 0;
}

struct Instr *
parseInstruction()
{
    enum TokenKind op = tokenKind;

    if (op < ADDQ || op > TRAP) {
	return 0;
    }

    getToken();


    struct TokenLoc loc = tokenLoc;
    struct InstrOperand *operand = 0;
    switch (op) {

	case NOP:
	    return makeInstr(op, 0);

	case GETC:
	    operand = parseOperands_R();
	    if (! operand) {
		parseError(&loc, EXPECTED_1ADDR_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);

	case HALT:
	case PUTC:
	    operand = parseOperands_I();
	    if (! operand) {
		operand = parseOperands_R();
	    }
	    if (! operand) {
		parseError(&loc, EXPECTED_1ADDR_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);

	case JA:
	case JNBE:
	case JAE:
	case JNB:
	case JNAE:
	case JB:
	case JNA:
	case JBE:
	case JE:
	case JZ:
	case JG:
	case JNLE:
	case JGE:
	case JNL:
	case JL:
	case JNGE:
	case JLE:
	case JNG:
	case JNE:
	case JNZ:
	    operand = parseOperands_I();
	    if (! operand) {
		parseError(&loc, EXPECTED_1ADDR_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);

	case JMP:
	    operand = parseOperands_I();
	    if (! operand) {
		operand = parseOperands_RR();
	    }
	    if (! operand) {
		parseError(&loc, EXPECTED_2ADDR_OR_1ADDR_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);
	
	case NOTQ:
	    operand = parseOperands_RR();
	    if (! operand) {
		parseError(&loc, EXPECTED_2ADDR_OR_1ADDR_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);

	case ANDQ:
	case ORQ:
	case TRAP:
	    operand = parseOperands_RRR();
	    if (! operand) {
		parseError(&loc, EXPECTED_3ADDR_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);

	case ADDQ:
	case DIVQ:
	case IDIVQ:
	case IMULQ:
	case MULQ:
	case SALQ:
	case SHLQ:
	case SARQ:
	case SHRQ:
	case SUBQ:
	    operand = parseOperands_IRR();
	    if (! operand) {
		operand = parseOperands_RRR();
	    }
	    if (! operand) {
		parseError(&loc, EXPECTED_3ADDR_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);

	case LDSWQ:
	case LDZWQ:
	case SHLDWQ:
	    operand = parseOperands_IR();
	    if (! operand) {
		parseError(&loc, EXPECTED_2ADDR_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);

	case MOVB:
	case MOVL:
	case MOVW:
	    operand = parseOperands_RM();
	    if (! operand) {
		parseError(&loc, EXPECTED_STORE_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);


	case MOVQ:
	    operand = parseOperands_MR();
	    if (! operand) {
		operand = parseOperands_RM();
	    }
	    if (! operand) {
		parseError(&loc, EXPECTED_FETCH_OR_STORE_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);

	case MOVSBQ:
	case MOVZBQ:
	case MOVSLQ:
	case MOVZLQ:
	case MOVSWQ:
	case MOVZWQ:
	    operand = parseOperands_MR();
	    if (! operand) {
		parseError(&loc, EXPECTED_FETCH_OPERAND, &op);
		return 0;
	    }
	    return makeInstr(op, operand);
	
	default:
	    parseError(&loc, OP_NOT_IMPLEMENTED, 0);
	    stop(2);
	    return 0;
    }
    return 0;
}

struct InstrOperand *
parseOperands_I()
{
    struct Expr *expr0 = parseImmediate();
    if (! expr0) {
	return 0;
    }
    return make1AddrOperand(FMT_I, expr0);
}

struct InstrOperand *
parseOperands_R()
{
    struct Expr *expr0 = parseRegister();
    if (! expr0) {
	return 0;
    }
    return make1AddrOperand(FMT_R, expr0);
}

struct InstrOperand *
parseOperands_RR()
{
    // first operand (register)
    struct Expr *expr0 = parseRegister();
    if (! expr0) {
	return 0;
    }

    // second operand (register)
    if (! expected(COMMA)) {
	return 0;
    }
    struct TokenLoc loc = tokenLoc;
    struct Expr *expr1 = parseRegister();
    if (! expr1) {
	parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
	return 0;
    }
    return make2AddrOperand(FMT_RR, expr0, expr1);
}

struct InstrOperand *
parseOperands_IRR()
{
    // first operand (immediate)
    struct Expr *expr0 = parseImmediate();
    if (! expr0) {
	return 0;
    }

    // second operand (register)
    if (! expected(COMMA)) {
	return 0;
    }
    struct TokenLoc loc = tokenLoc;
    struct Expr *expr1 = parseRegister();
    if (! expr1) {
	parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
	return 0;
    }

    // third operand (register)
    if (! expected(COMMA)) {
	return 0;
    }
    loc = tokenLoc;
    struct Expr *expr2 = parseRegister();
    if (! expr2) {
	parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
	return 0;
    }
    return make3AddrOperand(FMT_IRR, expr0, expr1, expr2);
}

struct InstrOperand *
parseOperands_IR()
{
    // first operand (immediate)
    struct Expr *expr0 = parseImmediate();
    if (! expr0) {
	expr0 = parseWordOp();
    }
    if (! expr0) {
	return 0;
    }

    // second operand (register)
    if (! expected(COMMA)) {
	return 0;
    }
    struct TokenLoc loc = tokenLoc;
    struct Expr *expr1 = parseRegister();
    if (! expr1) {
	parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
	return 0;
    }
    return make2AddrOperand(FMT_IR, expr0, expr1);
}

struct InstrOperand *
parseOperands_RRR()
{
    // first operand (register)
    struct Expr *expr0 = parseRegister();
    if (! expr0) {
	return 0;
    }

    // second operand (register)
    if (! expected(COMMA)) {
	return 0;
    }
    struct TokenLoc loc = tokenLoc;
    struct Expr *expr1 = parseRegister();
    if (! expr1) {
	parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
	return 0;
    }

    // third operand (register)
    if (! expected(COMMA)) {
	return 0;
    }
    loc = tokenLoc;
    struct Expr *expr2 = parseRegister();
    if (! expr2) {
	parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
	return 0;
    }
    return make3AddrOperand(FMT_RRR, expr0, expr1, expr2);
}

struct InstrOperand *
parseOperands_M()
{
    struct TokenLoc loc = tokenLoc;
    struct Expr *displace = parseExpression();
    if (! displace && tokenKind != LPAREN_MEM) {
	return 0;
    }
    if (! expected(LPAREN_MEM)) {
	return 0;
    }
    loc = tokenLoc;
    struct Expr *base = parseExpression();
    if (! base) {
	parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
	return 0;
    }
    struct Expr *index = 0;
    struct Expr *scale = 0;
    if (! displace) {
	if (tokenKind == COMMA) {
	    getToken();
	    loc = tokenLoc;
	    index = parseRegister();
	    if (! index) {
		parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
		return 0;
	    }

	    if (tokenKind == COMMA) {
		getToken();
		loc = tokenLoc;
		scale = parseExpression();
		if (! scale) {
		    parseError(&loc, EXPECTED_SCALE_OPERAND, 0);
		    return 0;
		}
	    } else {
		scale = makeValExpr(&loc, ABSOLUTE, 1, "1");
	    }
	}
    }
    if (! expected(RPAREN)) {
	return 0;
    }
    if (! index && !displace) {
	// memory operand (%X) becomes 0(%X)
	displace = makeValExpr(&loc, ABSOLUTE, 0, "0");
    }
    return make5AddrOperand(0, displace, base, index, scale, 0);
}

struct InstrOperand *
parseOperands_MR()
{
    struct InstrOperand *operand = parseOperands_M();
    if (! operand) {
	return 0;
    }
    if (! expected(COMMA)) {
	return 0;
    }
    struct TokenLoc loc = tokenLoc;
    struct Expr *dest = parseRegister();
    if (! dest) {
	parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
	return 0;
    }
    fixFetchOperand(operand, dest);
    return operand;
}

struct InstrOperand *
parseOperands_RM()
{
    struct Expr *src = parseRegister();
    if (! src) {
	return 0;
    }
    if (! expected(COMMA)) {
	return 0;
    }
    struct TokenLoc loc = tokenLoc;
    struct InstrOperand *operand = parseOperands_M();
    if (! operand) {
	parseError(&loc, EXPECTED_MEMORY_OPERAND, 0);
	return 0;
    }
    fixStoreOperand(operand, src);
    return operand;
}

struct Expr *
parseWordOp()
{
    if (tokenKind < W0 || tokenKind > W3) {
	return 0;
    }
    enum ExprOp op = OP_W0 + tokenKind - W0;
    getToken();
    struct TokenLoc loc = tokenLoc;
    if (tokenKind != LPAREN) {
	parseError(&loc, EXPECTED_LPAREN, 0);
	return 0;
    }
    getToken();

    loc = tokenLoc;
    struct Expr *expr = parseExpression();
    if (! expr) {
	parseError(&loc, EXPECTED_EXPRESSION, 0);
	return 0;
    }
    if (tokenKind != RPAREN) {
	parseError(&loc, EXPECTED_RPAREN, 0);
    }
    getToken();
    return makeUnaryExpr(&loc, op, expr);
}

struct Expr *
parseImmediate()
{
    bool withDollar = false;
    if (tokenKind == DOLLAR) {
	withDollar = true;
	getToken();
    }

    struct TokenLoc loc = tokenLoc;
    struct Expr *expr = parseExpression();
    if (! expr) {
	if (withDollar) {
	    parseError(&loc, EXPECTED_EXPR_AFTER_DOLLAR, 0);
	}
	return 0;
    }
    return expr;
}

struct Expr *
parseRegister()
{
    if (tokenKind != PERCENT) {
	return false;
    }
    getToken();

    struct TokenLoc loc = tokenLoc;
    struct Expr *expr = parseExpression();
    if (! expr) {
	parseError(&loc, EXPECTED_REGISTER_OPERAND, 0);
	return 0;
    }
    return expr;
}

struct Expr *
parseExpression()
{
    struct Expr *left = parseTerm();
    if (! left) {
	return 0;
    }
    enum TokenKind opToken = tokenKind;
    if (opToken == PLUS || opToken == MINUS) {
	enum ExprOp op = opToken - PLUS + ADD;
	getToken();
	struct TokenLoc loc = tokenLoc;
	struct Expr *right = parseExpression();
	if (! right) {
	    parseError(&loc, EXPECTED_TERM, 0);
	    return 0;
	}
	return makeBinaryExpr(op, left, right);
    }
    return left;
}

struct Expr *
parseTerm()
{
    struct Expr *left = parseFactor();
    if (! left) {
	return 0;
    }
    enum TokenKind opToken = tokenKind;
    
    if (opToken == ASTERISK || opToken == SLASH || opToken == PERCENT) {
	enum ExprOp op = opToken - PLUS + ADD;
	getToken();
	struct TokenLoc loc = tokenLoc;
	struct Expr *right = parseTerm();
	if (! right) {
	    parseError(&loc, EXPECTED_TERM, 0);
	    return 0;
	}
	return makeBinaryExpr(op, left, right);
    }
    return left;
}

struct Expr *
parseFactor()
{
    if (tokenKind == IDENT) {
	const char *ident = addString(tokenValue);
	struct TokenLoc loc = tokenLoc;
	getToken();

	
	struct Expr *found = getSym(ident);
	if (! found) {
	    struct Expr *unknown = makeValExpr(0, UNKNOWN, 0, "0");
	    addSym(ident, unknown);
	} else if (found->type == ABSOLUTE) {
	    return makeValExpr(&loc, ABSOLUTE, evalExpression(found), ident);
	}
	return makeIdentExpr(&loc, ident);
    } else if (tokenKind == CHARACTER_LITERAL) {
	uint64_t val = *processedTokenValue;
	struct Expr *expr = makeValExpr(&tokenLoc, ABSOLUTE, val,
					addString(tokenValue));
	getToken();
	return expr;
    } else if (tokenKind == OCTAL_LITERAL) {
	uint64_t val;
	if (! octStr_to_uint64_t(tokenValue, &val)) {
	    parseWarning(&tokenLoc, OVERFLOW_OCTAL_LITERAL, 0);
	}
	struct Expr *expr = makeValExpr(&tokenLoc, ABSOLUTE, val,
					addString(tokenValue));
	getToken();
	return expr;
    } else if (tokenKind == HEXADECIMAL_LITERAL) {
	uint64_t val;
	if (! hexStr_to_uint64_t(tokenValue, &val)) {
	    parseWarning(&tokenLoc, OVERFLOW_DECIMAL_LITERAL, 0);
	}
	struct Expr *expr = makeValExpr(&tokenLoc, ABSOLUTE, val,
					addString(tokenValue));
	getToken();
	return expr;
    } else if (tokenKind == DECIMAL_LITERAL) {
	uint64_t val;
	if (! decStr_to_uint64_t(tokenValue, &val)) {
	    parseWarning(&tokenLoc, OVERFLOW_DECIMAL_LITERAL, 0);
	}
	struct Expr *expr = makeValExpr(&tokenLoc, ABSOLUTE, val,
					addString(tokenValue));
	getToken();
	return expr;
    } else if (tokenKind == MINUS) {
	getToken();
	struct TokenLoc loc = tokenLoc;
	struct Expr *expr = parseFactor();
	if (! expr) {
	    parseError(&loc, EXPECTED_FACTOR_AFTER_UNARY_MINUS, 0);
	    return 0;
	}
	return makeUnaryExpr(&loc, NEG, expr);
    } else if (tokenKind == PLUS) {
	getToken();
	struct TokenLoc loc = tokenLoc;
	struct Expr *expr = parseFactor();
	if (! expr) {
	    parseError(&loc, EXPECTED_FACTOR_AFTER_UNARY_PLUS, 0);
	    return 0;
	}
	return expr;
    } else if (tokenKind == LPAREN) {
	getToken();
	struct TokenLoc loc = tokenLoc;
	struct Expr *expr = parseExpression();
	if (! expr) {
	    parseError(&loc, EXPECTED_EXPRESSION, 0);
	    return 0;
	}
	if (tokenKind != RPAREN) {
	    parseError(&loc, EXPECTED_RPAREN, 0);
	    return 0;
	}
	getToken();
	return expr;
    }
    return 0;
}
