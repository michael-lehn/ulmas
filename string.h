#ifndef ULMAS_STRING_H
#define ULMAS_STRING_H

#include <stddef.h>

/**
  * Checks if string str is already stored in a string pool. If not, it gets
  * added (the length of the string and its content are stored internally).
  * Returns a pointer to the internally stored string.
  *
  * Hence, for comparing stored strings you don't have to use strcmp, just
  * compare pointers.
  **/
const char *addString(const char *str);

/**
  * Returns the length of a string.
  */
size_t getStringLen(const char *str);

void printStringPool();

#endif // ULMAS_STRING_H
