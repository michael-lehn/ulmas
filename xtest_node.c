#include "expr.h"

int
main()
{
    struct Expr *a = makeValExpr(0, ABSOLUTE, 4, "4");
    struct Expr *b = makeValExpr(0, ABSOLUTE, 5, "5");
    struct Expr *s = makeBinaryExpr(ADD, a, b);

    fprintExpr(stdout, s);
}
