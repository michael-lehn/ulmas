#include <stdio.h>

#include "memregion.h"

int
main()
{
    for (size_t i = 0; i < 8; ++i) {
	char *a = alloc(DATA, sizeof(*a));

	printf("a = %p\n", a);
    }

    printf("dealloc\n");
    dealloc(DATA);

    for (size_t i = 0; i < 16; ++i) {
	char *a = alloc(DATA, sizeof(*a));

	printf("a = %p\n", a);
    }
}
