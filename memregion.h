#ifndef ULMAS_MEMREGION_H
#define ULMAS_MEMREGION_H 1

#include <stddef.h>

enum MemRegion {
    TEXT,
    DATA,
    BSS,
    EXPR,
    STRING,
    SYMTAB,
    FIXUP,
    MEMREGION_
};

void *alloc(enum MemRegion memRegion, size_t numBytes);

void dealloc(enum MemRegion memRegion);

void printMemRegions();

#endif // ULMAS_MEMREGION_H
