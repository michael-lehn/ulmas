#include <stddef.h>
#include <stdio.h>

#include "memregion.h"
#include "string.h"

static ptrdiff_t
strcmp_(const char *s1, const char *s2)
{
    for (; *s1 && *s1 == *s2; ++s1, ++s2) {
    }
    return *s1 - *s2;
}

static void
strcpy_(char *dst, const char *src)
{
    while ((*dst++ = *src++)) {
    }
}

static size_t
strlen_(const char *s)
{
    const char *cp;
    for (cp = s; *cp; ++cp) {
    }
    return cp - s;
}

struct Entry;

struct EntryHeader
{
    struct Entry *next;
    size_t len;
};

struct Entry
{
    struct EntryHeader h;
    char str[];
};

static struct Entry *stringList;

const char *
addString(const char *s)
{
    size_t len = strlen_(s);

    // search
    for (struct Entry *entry = stringList; entry; entry = entry->h.next) {
	if (len == entry->h.len && !strcmp_(s, entry->str)) {
	    return entry->str;
	}
    }

    // insert if not found
    struct Entry *entry = alloc(STRING, len + sizeof(struct EntryHeader) + 1);
    entry->h.len = len;
    strcpy_(entry->str, s);
    entry->h.next = stringList;
    stringList = entry;

    return entry->str;
}

size_t
getStringLen(const char *str)
{
    return ((struct Entry *)(str - sizeof(struct EntryHeader)))->h.len;
}

void
printStringPool()
{
    for (struct Entry *entry = stringList; entry; entry = entry->h.next) {
	printf("'%s' len = %zu at %p\n", entry->str, getStringLen(entry->str),
	       entry->str);
    }
}
